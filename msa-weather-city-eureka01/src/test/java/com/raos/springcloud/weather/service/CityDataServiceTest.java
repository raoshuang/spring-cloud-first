package com.raos.springcloud.weather.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertSame;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/10 23:20
 * 城市数据服务接口
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CityDataServiceTest {

	@Autowired
	private CityDataService cityDataServiceImpl;

	@Test
	public void testListCity() throws Exception {
		assertSame("City Data Count:", 110, cityDataServiceImpl.listCity().size());
	}
}