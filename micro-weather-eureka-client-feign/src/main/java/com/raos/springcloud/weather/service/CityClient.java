package com.raos.springcloud.weather.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/12 21:36
 * 访问城市信息的客户端.
 */
@FeignClient("msa-weather-city-eureka01")
public interface CityClient {
    @GetMapping("/cities")
    String listCity();
}
