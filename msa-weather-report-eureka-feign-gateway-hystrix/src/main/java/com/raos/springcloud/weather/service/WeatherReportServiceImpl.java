package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.Weather;
import com.raos.springcloud.weather.vo.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/17 23:41
 * 天气预报服务实现.
 */
@Service
public class WeatherReportServiceImpl implements WeatherReportService {

	@Autowired
	private DataClient dataClient;

	@Override
	public Weather getDataByCityId(String cityId) {
		// 由天气数据API微服务来提供数据
		WeatherResponse weatherResponse = dataClient.getDataByCityId(cityId);
		return weatherResponse.getData();
	}

}
