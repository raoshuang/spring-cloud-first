package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.Weather;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/17 23:35
 * 天气预报服务接口.
 */
public interface WeatherReportService {

	/**
	 * 根据城市ID查询天气信息
	 * @param cityId 城市ID
	 */
	Weather getDataByCityId(String cityId);

}
