package com.raos.springcloud;

import com.raos.springcloud.weather.WeatherFeignGatewayHystrix;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/17 23:12
 * 主应用测试入口
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WeatherFeignGatewayHystrix.class)
class ApplicationTests {

	@Test
	void contextLoads() {
	}

}
