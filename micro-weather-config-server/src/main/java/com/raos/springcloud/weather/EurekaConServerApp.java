package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/17 21:04
 * 基于Gradle构建的Spring Boot Eureka-ConfigServerApp入口
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer
public class EurekaConServerApp {

	public static void main(String[] args) {
		SpringApplication.run(EurekaConServerApp.class, args);
	}

}
