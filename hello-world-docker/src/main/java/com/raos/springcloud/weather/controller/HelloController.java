package com.raos.springcloud.weather.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/16 21:55
 * 基于Gradle构建的Spring Boot 的 HelloWorld Controller
 */
@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String hello() {
        return "Hello World! Welcome to visit Gradle, Spring Boot!";
    }
}
