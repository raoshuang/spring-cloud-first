package com.raos.springcloud.weather.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/16 23:25
 * 基于Gradle构建的Spring Boot 的 HelloWorld Controller
 * 日志采集验证
 */
@RestController
public class HelloController {
    private static final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @RequestMapping("/hello")
    public String hello() {
        logger.info("hello world");
        return "Hello World! Welcome to visit Gradle, Spring Boot!";
    }

}
