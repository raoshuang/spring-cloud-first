package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/16 23:52
 * 基于Gradle构建的Spring Boot HelloWorld工程入口
 * 测试微服务日志采集与监控
 */
@SpringBootApplication
public class LogApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogApplication.class, args);
	}

}
