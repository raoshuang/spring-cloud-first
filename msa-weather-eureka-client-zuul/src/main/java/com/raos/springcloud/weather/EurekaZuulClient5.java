package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/13 21:40
 * 基于Gradle构建的Spring Boot天气预报微服务Eureka--Zuul网关客户端入口
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class EurekaZuulClient5 {

	public static void main(String[] args) {
		SpringApplication.run(EurekaZuulClient5.class, args);
	}

}
