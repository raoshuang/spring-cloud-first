package com.raos.springcloud.weather.service;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/10 21:03
 * 天气数据采集服务.
 */
public interface WeatherDataCollectionService {
	
	/**
	 * 根据城市ID同步天气数据
	 * @param cityId 城市ID
	 */
	void syncDataByCityId(String cityId);

}
