package com.raos.springcloud;

import com.raos.springcloud.weather.EurekaConfigClientApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/17 21:32
 * 主应用测试入口
 * 测试配置信息集中化
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EurekaConfigClientApp.class) // 指明springboot入口，否则类名为入口类名+Tests
public class ApplicationTests {

	@Value("${auther}")
	private String auther;

	@Test
	public void contextLoads() {
		assertEquals("waylau.com", auther);
	}

}
