# 第3章 Spring Boot 的高级主题
## 本章知识
    3-1、构建 RESTful 服务
    3-2、Spring Boot 的配置详解
    3-3、内嵌 Servlet 容器
    3-4、实现安全机制
    3-5、允许跨域访问
    3-6、消息通信
    3-7、数据持久化
    3-8、实现热插拔（hot swapping）

## 3-1、构建 RESTful 服务
### 3-1-1、RESTful服务概述
        RESTful服务（也称为REST Web服务，RESTful WebServices）是松耦合的，特别适用于为客户创建在互联网传播的轻量级的Web服务API。
    在RESTful服务中，我们经常会将资源以JSON或XML等轻量级的数据格式进行暴露，从而可以方便地让其他REST客户端进行调用。
    在Java领域中有非常多的框架，可以帮助我们快速实现RESTful服务，主要分为基于JAX-RS的RESTful服务和基于Spring MVC的RESTful服务。
    
    1、基于JAX-RS的RESTful服务
        在JavaEE规范中，针对构建RESTful服务，主要是JAX-RS(Java APl for RESTful Web Services），该规范使Java程序员可以使用
    一套固定的接口来开发REST应用，避免了依赖于第三方框架。同时，JAX-RS使用POJO编程模型和基于标注的配置，并集成了JAXB，
    从而可以有效缩短REST应用的开发周期。
    
    2、基于Spring MVC的 RESTful服务
        Spring MVC框架本身也是可以实现RESTful服务的，只是并未实现JAX-RS规范。在SpringBoot应用中，通常采用Spring MVC来实现RESTful服务。
    当然Spring Boot本身也是支持对JAX-RS实现的集成。
        Spring MVC对于RESTful的支持，主要通过以下注解来实现。
        1）@Controller：声明为请求处理控制器。
        2）@RequestMapping：请求映射到相应的处理方法上。该注解又可以细化为以下几种类型:
            -@GetMapping；
            -@PostMapping；
            -@PutMapping；
            -@DeleteMapping；
            -@PatchMapping。
        3）@ResponseBody：响应内容的转换，如转换成JSON格式。
        4）@RequestBody：请求内容的转换，如转换成JSON格式。
        5）@RestController：等同于@Controller + @ResponseBody，方便处理RESTful的服务请求。

### 3-1-2、配置环境
    开发环境:
    1）JDK8;
    2）Gradle 5.6;
    3）Spring Boot Web Starter 2.2.12.RELEASE。
        Spring Boot Web Starter集成了Spring MVC，可以方便地来构建RESTful Web应用，并使用Tomcat作为默认的内嵌Servlet容器。

### 3-1-3、需求分析及API设计
    在本节，主要实现一个简单版本的“用户管理”RESTful服务。通过“用户管理”的API，方便简单地进行用户的增、删、改、查等操作。
    用户管理的整体API设计如下:
        1）GET       /users：     获取用户列表;
        2）POST      /users：     保存用户;
        3）GET       /users/{id}：获取用户信息;
        4）PUT       /users/{id}：修改用户;
        5）DELETE    /usersl{id}：删除用户。
    这样，相应的控制器可以定义如下。
        @RestController
        @RequestMapping("/users")
        public class UserController {
        	@Autowired
        	private UserRepository userRepository;
        
        	/**
        	 * 获取用户列表
        	 */
        	@GetMapping
        	public List<User> getUsers() {
        		return userRepository.listUser();
        	}
        	/**
        	 * 获取用户信息
        	 */
        	@GetMapping("/{id}")
        	public User getUser(@PathVariable("id") Long id) {
        		return userRepository.getUserById(id);
        	}
        	/**
        	 * 保存用户
        	 */
        	@PostMapping
        	public User createUser(@RequestBody User user) {
        		return userRepository.saveOrUpateUser(user);
        	}
        	/**
        	 * 修改用户
        	 */
        	@PutMapping("/{id}")
        	public void updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        		User oldUser = this.getUser(id);
        		if (oldUser != null) {
        			user.setId(id);
        			userRepository.saveOrUpateUser(user);
        		}
        	}
        	/**
        	 * 删除用户
        	 */
        	@DeleteMapping("/{id}")
        	public void deleteUser(@PathVariable("id") Long id) {
        		userRepository.deleteUser(id);
        	}
        }

### 3-1-4、项目配置
    在之前讲述的“hello-world”应用的基础上，我们稍作修改来生成一个新的应用。新的应用称为“springboot-rest02”。
    由于在build.gradle文件中已经配置了Spring Boot WebStarter，所以并不需要做特别的修改。
        dependencies {
        	implementation 'org.springframework.boot:spring-boot-starter-web'
        	testImplementation('org.springframework.boot:spring-boot-starter-test') {
        		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
        	}
        	testCompile group: 'junit', name: 'junit', version: '4.12'
        }

### 3-1-5、编写程序代码
    下面进行后台编码实现，编码涉及实体类、仓库接口、仓库实现类及控制器类。
    1、实体类
        在com.raos.springcloud.weather.domain包下，用于放置实体类。我们定义一个保存用户信息的实体User。实体属性如示。
            private Long id;
            private String name;
            private String email;
    2、仓库接口及实现
        在com.raos.springcloud.weather.repository包下，用于放置仓库接口及其仓库实现类，也就是我们的数据存储。
        1）用户仓库接口UserRepository如下。
            public interface UserRepository {
            	/**
            	 * 新增或者修改用户
            	 */
            	User saveOrUpateUser(User user);
            	/**
            	 * 删除用户
            	 */
            	void deleteUser(Long id);
            	/**
            	 * 根据用户id获取用户
            	 */
            	User getUserById(Long id);
            	/**
            	 * 获取所有用户的列表
            	 */
            	List<User> listUser();
            }
        2）UserRepository的实现类如示。
            @Repository
            public class UserRepositoryImpl implements UserRepository {
            	private static AtomicLong counter = new AtomicLong();
            	private final ConcurrentMap<Long, User> userMap = new ConcurrentHashMap<Long, User>();
            	@Override
            	public User saveOrUpateUser(User user) {
            		Long id = user.getId();
            		if (id == null || id <= 0) {
            			id = counter.incrementAndGet();
            			user.setId(id);
            		}
            		this.userMap.put(id, user);
            		return this.getUserById(id);
            	}
            	@Override
            	public void deleteUser(Long id) {
            		this.userMap.remove(id);
            	}
            	@Override
            	public User getUserById(Long id) {
            		return this.userMap.get(id);
            	}
            	@Override
            	public List<User> listUser() {
            		return new ArrayList<User>(this.userMap.values());
            	}
            }            
        其中，我们用ConcurrentMap<Long，User> userMap来模拟数据的存储，AtomicLong counter用来生成一个递增的ID，作为用户的唯一编号。
    @Repository注解用于标识UserRepositoryImpl类是一个可注入的bean。
    3、控制类
        在com.raos.springcloud.weather.controller包下，用于放置控制器类，也就是我们需要实现的API。
        UserController实现见 3-1-3、需求分析及API设计
            
### 3-1-6、安装REST客户端
        为了测试REST接口，我们需要一款REST客户端。
        有非常多的REST客户端可供选择，例如，Chrome浏览器的 Postman插件，或者是Firefox浏览器的RESTClient及HttpRequester插件，
    都能方便用于RESTful API的调试。
        这里这里使用Postman。

### 3-1-7、运行、测试程序
    运行程序，项目启动在8080端口。
    首先，我们发送GET请求到http://localhost:8080/users，可以看到，响应返回的是一个空的列表[]。
    发送POST请求到http://localhost:8080/users，用来创建一个用户。请求内容为:
        { "name":"raos", "email":"raos991207823@qq.com" }
    发送成功，能看到响应状态是200，响应内容：
        {
            "id": 1,
            "name": "raos",
            "email": "raos991207823@qq.com"
        }
    我们通过该接口，再创建几条测试数据，并发送GET请求到http://localhost:8080/users，可以看到，响应返回的是一个有数据的列表。
        [
            {
                "id": 1,
                "name": "raos",
                "email": "raos991207823@qq.com"
            },
            {
                "id": 2,
                "name": "zhangs",
                "email": "zhangs123@qq.com"
            }
        ]
    我们通过PUT方法到http:/localhost:8080/users/2，来修改id为2的用户信息，修改为如下内容。
        { "name":"柳伟卫", "email" :"778907484@qq.com" }
    发送成功，我们能看到响应的状态是200。我们通过GET方法到 http:/localhost:8080/users/2来查看id为2的用户信息:
        {
            "id": 2,
            "name": "柳伟卫",
            "email": "778907484@qq.com"
        }
    可以看到，用户数据已经变更了。自此这个简单的“用户管理”的RESTful服务已经全部调试完毕。

## 3-2、SpringBoot的配置详解
### 3-2-1、理解SpringBoot的自动配置
        按照“约定大于配置”的原则，Spring Boot通过扫描依赖关系来使用类路径中可用的库。对于每个pom文件中的“spring-boot-starter-*”依赖，
    Spring Boot会执行默认的AutoConfiguration类。
        AutoConfiguration类使用*AutoConfiguration词法模式，其中*代表类库。例如，JPA存储库的自动配置是通过
    JpaRepositoriesAutoConfiguration来实现的。
        使用--debug运行应用程序可以查看自动配置的相关报告。下面的命令用于显示在3.1节中"spring-boot-rest02”应用的自动配置报告。
        $ java -jar build/libs/spring-boot-rest02-1.0.0.jar --debug
    以下是自动配置类的一些示例:
        1）ServerPropertiesAutoConfiguration;
        2）RepositoryRestMvcAutoConfiguration;
        3）JpaRepositoriesAutoConfiguration;
        4）JmsAutoConfiguration。
        如果应用程序有特殊的要求，比如需要排除某些库的自动配置，也是能够完全实现的。以下是排除DataSourceAutoConfiguration的示例。
    @EnableAutoConfiguration(exclude=(DataSourceAutoConfiguration.class})
    
### 3-2-2、重写默认的配置值
        也可以使用应用程序覆盖默认配置值。重写的配置值配置在application.properties文件中即可。比如，如果想更改应用启动的端口号，
    则可以在application.properties文件中添加如下内容。
        server.port=8081
    这样，这个应用程序再次启动时，就会使用端口8081。
    
### 3-2-3、更换配置文件的位置
    默认情况下，Spring Boot将所有配置外部化到application.properties文件中。但它仍然是应用程序构建的一部分。
    此外，可以通过设置来实现从外部读取属性。以下是设置的属性:
        1）spring.config.name：配置文件名;
        2）spring.config.location:配置文件的位置。
    这里，spring.config.location可以是本地文件位置。以下命令从外部启动Spring Boot应用程序来提供配置文件。
        $ java -jar build/libs/spring-boot-rest02-1.0.0.jar --spring.config.location=D:/Users/HASEE/SpringCloud/spring-boot-rest02/bootrest.properties

### 3-2-4、自定义配置
    开发者可以将自定义属性添加到application.properties文件中。
    例如，自定义一个名为“file.server.url”的属性在application.properties文件中。在Spring Boot启动后，我们就能将该属性自动注入应用中。
        @RestController
        @RequestMapping("/u")
        public class UserspaceController {
            @Value("${file.server.url}")
            private String fileServerUrl;
        
            @GetMapping( "/{username}/edit")
            public String createBlog (@PathVariable("username")String username) {
                Map<String, Object> data = new HashMap<>(8);
                data.put("fileServerUrl", fileServerUrl);
                data.put("username", username);
                return data.toString();
            }
        }

### 3-2-5、使用.yaml作为配置文件
        使用.yaml文件，实际是作为application.properties文件的一种替代方式。YAML与平面属性文件相比，提供了类似JSON的结构化配置。
    YAML数据结构可以用类似大纲的缩排方式呈现，结构通过缩进来表示，连续的项目通过减号“-”来表示，map结构里面的key/value对用冒号“:"来分隔。
    样例如下。
        spring:
            application:
                name: raos
            datasource:
                driverClassName:com. mysql.jdbc .Driver
                url:jdbc:mysql://localhost/test
        server:
            port: 8081

### 3-2-6、profiles的支持
    Spring Boot支持profiles，即不同的环境使用不同的配置。通常需要设置一个系统属性( spring.profiles.active)或
    OS环境变量（SPRING_PROFILES_ACTIVE)。例如使用-D参数启动应用程序（记住将其放在主类或jar之前）。
        $ java -jar -Dspring.profiles.active=production build/libs/spring-boot-rest02-1.0.0.jar
    在Spring Boot中，还可以在application.properties中设置激活的配置文件，例如，
        spring.profiles.active=production
    YAML文件实际上是由“---”行分隔的文档序列，每个文档分别解析为平坦化的映射。
     如果一个YAML文档包含一个spring.profiles键,那么配置文件的值(逗号分隔的配置文件列表)将被反馈到Spring Environment.acceptsProfiles()中，
    并且如果这些配置文件中的任何一个被激活，那么文档被包含在最终的合并中。例如：
        server:
            port:9000
        ---
        spring:
            profiles: development
        server:
            port:9001
        ---
        spring:
            profiles: production
        server:
            port: 0
    在此示例中，默认端口为9000，但是如果Spring profile的“development”处于激活状态，则端口为9001，如果“production”处于激活状态，则为0。
    要使.properties文件做同样的事情，可以使用“application-S{profile}.properties”的方式来指定特定于配置文件的值。

## 3-3、内嵌Servlet容器
    Spring Boot WebStarter内嵌了Tomcat服务器。在应用中使用嵌入式的Servlet容器，可以方便我们来进行项目的启动和调试。
    Spring Boot包括支持嵌入式Tomcat、Jetty和 Undertow服务器。默认情况下，嵌人式服务器将侦听端口8080上的HTTP请求。

### 3-3-1、注册Servlet、过滤器和监听器
        当使用嵌入式Servlet容器时，可以通过使用Spring bean或扫描Servlet组件从Servlet规范（如HttpSessionListener）中注册Servlet、
    过滤器和所有监听器。
        默认情况下，如果上下文只包含一个Servlet，它将映射到“”路径。在多个Servlet bean的情况下，bean名称将被用作路径前缀，
    过滤器将映射到“/*”路径。
        如果觉得基于惯例的映射不够灵活，可以使用ServletRegistrationBean、FilterRegistrationBean
    和ServletListenerRegistrationBean类进行完全控制。

### 3-3-2、Servlet 上下文初始化
        嵌入式Servlet容器不会直接执行Servlet 3.0+的javax.servlet.ServletContainerInitializer 接口或Spring 的
    org.springframework.web.WebApplicationInitializer 接口。这是因为在war中运行的第三方库会带来破坏Spring Boot应用程序的风险。
        如果您需要在Spring Boot应用程序中执行Servlet上下文初始化，则应注册一个实现org.springframework.boot.web.servlet.
    ServletContextInitializer接口的bean。onStartup方法提供对ServletContext的访问，并且如果需要，可以轻松地用作现有
    WebApplicationInitializer的适配器。
        当使用嵌人式容器时，可以使用@ServletComponentScan来自动注人启用了@WebServlet、@WebFilter和@WebListener注解的类。
        需要注意的是，@ServletComponentScan在独立部署的容器中不起作用，这是因为在独立部署的容器中将使用容器内置的发现机制。

### 3-3-3、ServletWebServerApplicationContext
        Spring Boot使用一种新型的ApplicationContext来支持嵌入式的Servlet容器。ServletWebServerApplicationContext
    就是这样一种特殊类型的WebApplicationContext，它通过搜索单个ServletWebServerFactory bean来引导自身。通常
    TomcatServletWebServerFactory、JettyServletWebServerFactory或 UndertowServletWebServerFactory将被自动配置。
        通常，开发者并不需要关心这些实现类。在大多数应用程序中将被自动配置，并将创建适当的ApplicationContext
    和ServletWebServerFactory。

### 3-3-4、更改内嵌Servlet容器
    SpringBoot Web Starter默认使用了Tomcat来作为内嵌的容器。在依赖中加入相应Servlet容器的Starter，就能实现默认容器的替换，
    如下所示。
        1）spring-boot-starter-jetty：使用Jetty作为内嵌容器，可以替换spring-boot-starter-tomcat.
        2）spring-boot-starter-undertow：使用Undertow作为内嵌容器，可以替换spring-boot-starter-tomcato。
            可以使用Spring Environment属性配置常见的Servlet容器的相关设置。通常将在application.
    properties文件中来定义属性。常见的Servlet容器设置如下所示。
        1）网络设置：监听HTTP请求的端口( server.port)、绑定到server.address的接口地址等。
        2）会话设置：会话是否持久(server.session.persistence)、会话超时(server.session.timeout)、会话数据的位置(server.session.store-dir)
            和会话cookie配置( server.session.cookie.*)。
        3）错误管理：错误页面的位置( server.error.path）等。
        4）SSL。
        5）HTTP压缩。
        SpringBoot尽可能地尝试公开这些常见公用设置，但也会有一些特殊的配置。对于这些例外的情况，SpringBoot 提供了专用命名空间
    来对应特定于服务器的配置（比如server.tomcat和server.undertow )。

## 3-4、实现安全机制
### 3-4-1、实现基本认证
        如果Spring Security位于类路径上，则所有HTTP端点上默认使用基本认证，这样就能使Web应用程序得到一定的安全保障。
    最为快捷的方式是在依赖中添加Spring Boot Security Starter。
        //依赖关系
        dependencies {
            //该依赖用于编译阶段
            compile('org.springframework.boot:spring-boot-starter-security')
            //...
        }
        如果要向Web应用程序添加方法级别的安全保障，还可以在Spring Boot应用里面添加@EnableGlobalMethodSecurity注解来实现，
    如下面的例子所示。
        @EnableGlobalMethodSecurity
        @SpringBootApplication
        public class Application{
            public static void main (String [] args） {
                SpringApplication.run(Application.class,args);
            }
        }
        需要注意的是，@EnableGlobalMethodSecurity可以配置多个参数：
        1）prePostEnabled：决定Spring Security 的前注解是否可用@PreAuthorize、@PostAuthorize等;
        2）secureEnabled：决定Spring Security的保障注解@Secured是否可用;
        3）jsr250Enabled:决定JSR-250注解@RolesAllowed等是否可用。
        配置方式分别如下。
        @EnableGlobalMethodSecurity(securedEnabled = true)
        public class MethodSecurityConfig {
            // ...
        }
        EnableGlobalMethodSecurity(jsr250Enabled = true)
        public class MethodSecurityConfig {
            // ...
        @EnableGlobalMethodSecurity(prePostEnabled = true)
        public class MethodSecurityConfig {
            // ...
        }
        在同一个应用程序中，可以启用多个类型的注解，但是对于行为类的接口或类只应该设置一个注解。如果将2个注解同时应用于某一特定方法，
    则只有其中一个被应用。

    1、@Secured
        此注解是用来定义业务方法的安全配置属性的列表。您可以在需要安全角色/权限等的方法上指定@Secured，并且只有那些角色/权限的用户
        才可以调用该方法。如果有人不具备要求的角色/权限但试图调用此方法，将会抛出AccessDenied异常。
        @Secured源于Spring之前的版本，它有一个局限就是不支持SpringEL表达式。可以看看下面的例子。
        如果你想指定AND(和）这个条件，即deleteUser方法只能被同时拥有ADMIN & DBA，但是仅仅通过使用@Secured注解是无法实现的。
        但是你可以使用Spring新的注解@PreAuthorize/@PostAuthorize(支持Spring EL)，使实现上面的功能成为可能，而且无限制。
            @PreAuthorize/@PostAuthorize
        Spring 的@PreAuthorize/@PostAuthorize注解更适合方法级的安全，也支持Spring EL表达式语言，提供了基于表达式的访问控制。
            1）@PreAuthorize注解:适合进入方法前的权限验证，@PreAuthorize可以将登录用户的角色/权限参数传到方法中。
            2）@PostAuthorize注解:使用并不多，在方法执行后再进行权限验证。
        以下是一个使用了@PreAuthorize注解的例子。
            @PreAuthorize("hasAuthority('ROLE ADMIN')")//指定角色权限才能操作方法
            @GetMapping(value ="delete/{id}")
            public ModelAndView delete (PathVariable ("id")Long id, Model model) {
                userService.removeUser(id）;
                model.addAttribute ("userList", userService.listUsers()）;
                model.addAttribute("title"，"删除用户");
                return new ModelAndView ("users/list", "userModel",model);
            }
    
    2、登录账号和密码
        默认的AuthenticationManager具有单个用户账号,其中用户名称是“user”，密码是一个随机码，在应用程序启动时以INFO级别打印，
        如下所示。
            Using default security password:78fa195d-3f4c-48bl-ad50-e24c31d5cf36
        当然，你也可以在配置文件中来自定义用户名和密码。
            security.user.name=guest
            security.user.password=guest123

### 3-4-2、实现OAuth2认证
    如果您的类路径中有spring-security-oauth2，则可以利用某些自动配置来轻松设置授权或资源服务器，即可实现OAuth2认证。
    1、什么是OAuth 2.0
        OAuth 2.0的规范可以参考RFC 6749 ( http://tools.ietf.org/html/rfc6749 )。OAuth是一个开放标准，允许用户让第三方应用
        访问该用户在某一网站上存储的私密的资源(如照片、视频、联系人列表等），而无须将用户名和密码提供给第三方应用。
        OAuth允许用户提供一个令牌,而不是用户名和密码来访问他们存放在特定服务提供者的数据。每一个令牌授权一个特定的网站
        （例如，视频编辑网站）在特定的时段（例如，接下来的2小时内)内访问特定的资源（例如，仅仅是某—相册中的视频）。
        这样OAuth允许用户授权第三方网站访问他们存储在另外的服务提供者上的信息，而不需要分享它们的访问许可或数据的所有内容。
    2、OAuth 2.0的核心概念
        OAuth 2.0主要有以下4类角色。
        1）resource owner：资源所有者，指终端的“用户”( user ) 。
        2）resource server：资源服务器，即服务提供商存放受保护资源。访问这些资源，需要获得访问令牌（Access Token )。
            它与认证服务器可以是同一台服务器，也可以是不同的服务器。如访问新浪博客网站，那么如果使用新浪博客的账号来登录新浪博客网站，
            那么新浪博客的资源和新浪博客的认证都是同一家，可以认为是同一个服务器。如果我们使用新浪博客账号去登录知乎，
            那么显然知乎的资源和新浪的认证不是一个服务器。
        3）client：客户端，代表向受保护资源进行资源请求的第三方应用程序。
        4）authorization server：授权服务器，在验证资源所有者并获得授权成功后，将发放访问令牌给客户端。
    3、OAuth 2.0的认证流程
        OAuth 2.0的认证流程如下。
            ①用户打开客户端以后，客户端请求资源所有者（用户）的授权。
            ②用户同意给予客户端授权。
            ③客户端使用上一步获得的授权，向认证服务器申请访问令牌。
            ④认证服务器对客户端进行认证以后，确认无误，同意发放访问令牌。
            ⑤客户端使用访问令牌，向资源服务器申请获取资源。
            ⑥资源服务器确认令牌无误，同意向客户端开放资源。
            其中，用户授权有以下4种模式:
                ·授权码模式(authorization code)；
                ·简化模式(implicit)；
                ·密码模式(resource owner password credentials)；
                ·客户端模式(client credentials)。
    4.配置
        项目的核心配置如下。
            github.client.clientId=ad2abbc19b6c5f0edl17
            github.client.clientSecret=26db88a4dfc34cebaf196e68761c1294ac4ce265
            github.client.accessTokenUri=https://github.com/login/oauth/access_token
            github.client.userAuthorizationUri=https://github.com/login/oauth/authorize
            github.client.clientAuthenticationScheme=form
            github.client.tokenName=oauth_token
            github. client.authenticationScheme=query
            github.resource.userInfoUri=https://api.github.com/user
        包括了作为一个client所需要的大部分参数。其中 clientld、clientSecret是在 GitHub注册一个应用时生成的。若是不想注册应用，
        则可以直接使用上面的配置。如果要注册，见后续注册流程。
    5、项目安全的配置
        安全配置中需要加上@EnableWebSecurity、@EnableOAuth2Client注解，来启用Web安全认证机制，并表明这是一个OAuth 2.0客户端。
        @EnableGlobalMethodSecurity注明，项目采用了基于方法的安全设置。
            @EnablewebSecurity
            @EnableOAuth2Client//启用OAuth2.0客户端
            EnableGlobalMethodSecurity(prePostEnabled = true)//启用方法安全设置
            public class SecurityConfig extends WebSecurityConfigurerAdapter{
                // ...
            }
        使用Spring Security，我们需要继承org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
        并重写以下configure方法。
            @override
            protected void configure (HttpSecurity http)throws Exception {
                http.addFilterBefore(ssoFilter(),BasicAuthenticationFilter.class)
                .antMatcher("/**")
                .authorizeRequests()
                .antMatchers("/","/index","/403","/css/**"，"/js/**"，"/fonts/**").permitAll()//不设限制，都允许访问
                .anyRequest()
                .authenticated()
                .and().logout().logoutSuccessUrl("/").permitAll()
                .and().csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttponlyFalse());
            }
        上面的配置是设置了一些过滤策略，除了静态资源及不需要授权的页面，我们允许访问其他的资源，都需要授权访问。
        其中设置了一个过滤器ssoFilter，用于在BasicAuthenticationFilter之前进行拦截。如果拦截到的是/login，就是访问认证服务器。
            private Filter ssoFilter() {
                OAuth2ClientAuthenticationProcessingFilter githubFilter= new 0Auth2ClientAuthenticationProcessingFilter("/login");
                OAuth2RestTemplate githubTemplate = new OAuth2RestTemplate(github(), oauth2ClientContext);
                githubFilter.setRestTemplate(githubTemplate) ;
                UserInfoTokenServices tokenServices = new UserInfoTokenServices(githubResource().getUserInfouri(), 
                    github().getClientId());
                tokenServices.setRestTemplate(githubTemplate);
                githubFilter.setTokenServices(tokenServices);
                return githubFilter;
            }
            @Bean
            public FilterRegistrationBean oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
                FilterRegistrationBean registration = new FilterRegistrationBean();
                registration.setFilter(filter);
                registration.setOrder(-100);
            return registration;
            @Bean
            @ConfigurationProperties("github.client")
            public AuthorizationCodeResourceDetails github() {
                return new AuthorizationCodeResourceDetails();
            }
            @Bean
            @configurationProperties("github.resource")
            public ResourceServerProperties githubResource(){
                return new ResourceserverProperties();
            }
    6、资源服务器
        我们写了两个控制器来提供相应的资源。
        一个控制器是MainController.java。代码如下。
            @controller
            public class MainController {
                GetMapping("/")
                public String root(）{
                    return"redirect:/index";
                }
                @GetMapping("/index")
                public String index(Principal principal, Model model){
                    if(principal == null) {
                        return"index";
                    }
                    System.out.println(principal.toString());
                    model.addAttribute("principal", principal);
                    return"index";
                }
                GetMapping("/403")
                public String accesssDenied() {
                    return" 403";
                }
            }
        在index页面如果认证成功，将会显示一些认证信息。
        另一个控制器是UserController.java，用来模拟用户管理的相关资源。
            @RestController
            @RequestMapping("/")
            public class UserController {
                /*
                *查询所用用户
                */
                @GetMapping("/users")
                @PreAuthorize("hasAuthority('ROLE_USER')")//指定角色权限才能操作方法
                public ModelAndView list(Model model) {
                    List<User> list = new ArrayList<>(); //当前所在页面数据列表
                    list.add(new User("waylau", 29));
                    list.add(new User("老卫", 30));
                    model.addAttribute("title", "用户管理");
                    model.addAttribute("userList"，list);
                    return new ModelAndView("users/list", "userModel", model);
                }
            }
    7、前端界面
        页面主要是采用Thymeleaf及Bootstrap来编写的。
        首页用于显示用户的基本信息。
            <body>
                <div class="container">
                    <div class="mt-3">
                        <h2>Hello Spring Security</h2>
                    </div>
                    <div sec:authorize="isAuthenticated()" th:if="${principal}" th:object="${principal}">
                        <p>已有用户登录</p>
                        <p>登录的用户为: <span sec:authentication="name"></span></p>
                        <p>用户权限为: <span th:text="* {userAuthentication.authorities}"></span></p>
                        <p>用户头像为: <img alt="" class="avatar width-full rounded-2" height="230" th:src="*{userAuthentication. details.avatar_ _url}" width="230"></p>
                    </div>
                    <div sec: authorize="isAnonymous()">
                        <p>未有用户登录</p>
                    </div>
                </div>
            </body>
        用户管理界面显示用户的列表。
            <body>
                <div class="container">
                    <div class="mt-3">
                        <h2 th:text="${userModel.title}">Welcome to waylau.com</h2>
                    </div>
                    <table class="table table-hover"
                    <thead>
                    <tr>
                        <td>Age</td>
                        <td>Name</td>
                        <td sec:authorize="hasRole('ADMIN')">0peration</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr th:if="${userModel.userList.size()} eq O">
                        <td colspan="3">没有用户信息!!</td>
                    </tr>
                        <tr th:each="user:${userModel.userList}">
                        <td th:text="${user.age}">11</td>
                        <td th:text="${user.name}">waylau</a></td>
                        <td sec:authorize="hasRole('ADMIN')">
                            <div>我是管理员</div>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
            </body>

    8、注册GitHub应用
        如果需要注册，请根据下面的流程来生成 Client ID和Client Secret。
        访问https:/lgithub.com/settings/applications/new，注册应用，生成客户端ID和密码。比如，
            Client ID:ad2abbc19b6c5f0edl17
            Client Secret:26db88a4dfc34cebaf196e68761c1294ac4ce265
        将客户端ID和密码写入程序配置即可。
### 3-4-3、示例源码
        源码参阅柳伟卫的《Spring Security 教程》附属源码地址：https://github.com/waylau/spring-security-tutorial

## 3-5、允许跨域访问
    CORS（Cross Origin Resource Sharing，跨域资源共享）机制允许Web应用服务器进行跨域访问控制，从而使跨域数据传输得以安全进行。
    浏览器支持在API容器中（如XMLHttpRequest或Fetch）使用CORS，以降低跨域HTTP请求所带来的风险。
### 3-5-1、什么是跨域访问
        当一个资源从与该资源本身所在的服务器不同的域或端口请求一个资源时，资源会发起一个跨域HTTP请求。
        比如，站点http;//example-a.com的某HTML页面通过<img的src请求http://example-b.comimage.jpg。网络上的许多页面都会加载
    来自不同域的CSS样式表、图像和脚本等资源。
        W3C制定了CORS的相关规范，见https:/www.w3.org/TR/corsl。出于安全考虑，浏览器会限制从脚本内发起的跨域HTTP请求。
    例如，XMLHttpRequest和Fetch遵循同源策略。因此，使用XMLHtpRequest或Fetch的Web应用程序只能将HTTP请求发送到其自己的域。
    为了改进Web应用程序，开发人员要求浏览器厂商允许跨域请求。
    
### 3-5-2、如何识别是跨域行为
        识别是否具有跨域行为，是由同源政策决定的。同源政策由Netscape公司引人浏览器。目前所有浏览器都实行这个政策。
    所谓“同源”，指的是“三个相同”。
        ·协议相同。
        ·域名相同。
        ·端口相同。
        举例来说，http://example.com/page.html这个网址，协议是http://，域名是www.example.com，端口是80（默认端口可以省略）。
    它的同源情况如下。
        ·http://example.com/other.html：     同源。
        ·http://www.example.com/other.html： 不同源，域名不同。
        ·http://v2.example.com/other.html：  不同源，域名不同。
        ·http://example.com:81/other.html：  不同源，端口不同。

### 3-5-3、在SpringBoot应用中允许跨域访问
        在微服务的架构里面，由于每个服务都在其自身的源中运行，因此，很容易就会遇到来自来源的客户端Web应用程序来访问服务的问题
    （即跨域访问）。例如，一个浏览器客户端从“客户”微服务器访问“客户”，并从“订单”微服务器访问订单历史记录，这种做法
    在微服务领域非常普遍。
        Spring MVC支持CORS的开箱即用的功能。主要有两种实现跨域访问的方式。
    1、方法级别的跨域访问
        Spring Boot 提供了一种简单的声明式方法来实现跨域请求。以下示例显示如何使用@CrossOrigin注解，来启用允许跨域访问某些接口。
            import org.springframework.web.bind.annotation.Cross0rigin;
            @Cross0rigin(origins = "*",maxAge =3600）//允许所有域名访问
            @Controller
            public class FileController {
                //....
            }
        其中，origins="*"紧意味着允许所有域名访问（当然也可以限定某个域名来访问）。maxAge=3600是指有效期为3600秒。
    2、全局跨域访问
        可以通过使用自定义的addCorsMappings(CorsRegistry)方法注册WebMvcConfigurer bean来定义全局CORS配置。用法如下。
            @Configuration
            public class MyConfiguration {
                @Bean
                public WebMvcConfigurer corsConfigurer() {
                    return new WebMvcConfigurer() {
                        @override
                        public void addCorsMappings(CorsRegistry registry) {
                            registry.addMapping("/api/**");
                        }
                    };
                }
            }

## 3-6、消息通信
        消息通信是企业信息集成中非常重要的一种方式。消息的通信一般是由消息队列系统（Message Queuing System，MQ）
    或面向消息中间件（Message Oriented Middleware，MOM）来提供高效可靠的消息传递机制进行平台无关的数据交流，并可基于数据通信
    进行分布式系统的集成。通过提供消息传递和消息排队模型，可在分布环境下扩展进程间的通信，并支持多种通信协议、语言、应用程序、
    硬件和软件平台。

### 3-6-1、消息通信的好处
        通过使用MQ或MOM，通信双方的程序（称其为消息客户程序）可以在不同的时间运行，程序不在网络上直接通话，而是间接地将消息
    放入MQ或MOM服务器的消息队列中。因为程序间没有直接的联系，所以它们不必同时运行：消息放入适当的队列时，目标程序不需要
    正在运行：即使目标程序在运行，也不意味着要立即处理该消息。
        消息客户程序之间通过将消息放入消息队列或从消息队列中取出消息来进行通信。客户程序不直接与其他程序通信，避免了网络通信
    的复杂性。消息队列和网络通信的维护工作由MQ或MOM完成。
        常见的MQ或MOM产品有Java Message Service、Apache ActiveMQ、Apache RocketMQ、RabbitMQ、Apache Kafka等。
        对于Spring应用而言，Spring Boot针对Java Message Service、RabbitMQ、Apache Kafka等提供了开箱即用的支持。

### 3-6-2、使用Java Message Service
    Java Message Service (JMS）API是一个Java面向消息中间件的API，用于两个或多个客之间发送消息。
    JMS的目标包括：
        ·包含实现复杂企业应用所需要的功能特性；
        ·定义了企业消息概念和功能的一组通用集合；
        ·最小化企业消息产品的概念，以降低学习成本。
        ·最大化消息应用的可移植性。
    JMS支持企业消息产品提供以下两种主要的消息风格。
        ·点对点(Point-to-Point，PTP）消息风格：允许一个客户端通过一个叫“队列( queue)”的中间抽象发送一个消息给另一个客户端。
            发送消息的客户端将一个消息发送到指定的队列中，接收消息的客户端从这个队列中抽取消息。
        ·发布订阅(Publish/Subscribe，Pub/Sub）消息风格：允许一个客户端通过一个叫“主题(topic)”的中间抽象发送一个消息
            给多个客户端。发送消息的客户端将一个消息发布到指定的主题中，然后这个消息将被投递到所有订阅了这个主题的客户端。
    在Spring Boot应用中使用JMS，通常需要以下几个步骤。
    1、使用JNDI ConnectionFactory
        在应用程序中，Spring Boot将尝试使用JNDI找到JMS ConnectionFactory。默认情况下，将检查位置java:/JmsXA和
    java:/XAConnectionFactory。如果需要指定其他位置，可以使用spring.jms.jndi-name属性。
        spring.jms.jndi-name=java:/MyConnectionFactory
    2、发送消息
        Spring 的JmsTemplate是自动配置的，可以将其直接自动装配到自己的bean中。
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.jms.core.JmsTemplate;
            import org.springframework.stereotype.Component;
            @Component
            public class MyBean {
                private final JmsTemplate jmsTemplate;
                @Autowired
                public MyBean(JmsTemplate jmsTemplate) {
                    this.jmsTemplate = jmsTemplate;
                }
                // ...
            }
    3、接收消息
        在JMS 架构中，可以使用@JmsListener来注解任何bean，以创建侦听器端点。如果没有定义JmsListenerContainerFactory，
    则会自动配置默认值。如果定义了DestinationResolver或Message-Converter bean，则它们将自动关联到默认工厂。
        默认工厂是事务性的。如果在JtaTransactionManager存在的基础架构中运行，则默认情况下将与侦听器容器相关联。如果没有，
    sessionTransacted标志将被启用。在后一种情况下，可以通过在侦听器方法（或其代理）上添加@Transactional来将本地数据存储事务
    关联到传入消息的处理。这将确保在本地事务完成后确认传入的消息。这还包括发送在同一个JMS会话上执行的响应消息。
    以下案例在someQueue目标上创建一个侦听器端点。
        @Component
        public class MyBean {
            @JmsListener(destination = "someQueue")
            public void processMessage(String content) {
                // ...
            }
        }

### 3-6-3、使用RabbitMQ
        RabbitMQ是更高级别的消息中间件,实现了Advanced Message Queuing Protocol(AMQP)协议。Spring AMQP项目将核心Spring概念
    应用于基于AMQP的消息传递解决方案的开发。Spring Boot提供了几种通过RabbitMQ与AMQP协同工作的开箱即用的方式，
    包括spring-boot-starter-amqp等各种Starter。
    1、配置RabbitMQ
        RabbitMQ的配置由外部配置属性 spring.rabbitmq.*来控制。例如可以在 application.properties中声明以下部分。
            spring.rabbitmq.host=localhost
            spring.rabbitmq.port=5672
            spring.rabbitmq.username=admin
            spring.rabbitmg.password=secret
    2、发送消息
        Spring 的AmapTemplate和 AmqpAdmin是自动配置的，可以将它们直接自动装配到自己的bean中。
            import org.springframework.amqp.core.AmepAdmin;
            import org.springframework.amap.core.AmqpTemplate;
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.stereotype.Component;
            @Component
            public class MyBean {
                private final AmapAdmin amqpAdmin;
                private final AmgpTemplate amgpTemplate;
                @Autowired
                public MyBean (AmapAdmin amapAdmin,Amgp'Template amapTemplate){
                    this.amgpAdmin = amgpAdmin;
                    this.amgpTemplate= amqpTemplate;
                }
                // ...
            }
    3、接收消息
        当Rabbit的基础架构存在时，可以使用@RabbitListener来注解bean，以创建侦听器端点。如果没有定义RabbitListenerContainerFactory，
    则会自动配置默认的SimpleRabbitListenerContainerFactory。可以使用spring.rabbitmq.listener.type属性切换到直接容器。
    如果MessageConverter或MessageRecoverer bean被定义，它们将自动关联到默认工厂。
        以下示例是在someQueue队列上创建一个侦听器端点。
            @Component
            public class MyBean {
                @RabbitListener(queues = "someQueue")
                public void processMessage(String content) {
                    // ...
                }
            }

## 3-7、数据持久化
        JPA(Java Persistence API）是用于管理JavaEE和Java SE环境中的持久化，以及对象/关系映射的Java API。
        目前市面上实现该规范的常见JPA框架有EclipseLink(http://www.eclipse.org/eclipselink)、Hibernat(http://hiberate.org/orm)、
    Apache OpenJPA(http://openjpa.apache.org/)等。本次学习主要介绍以Hibernat为实现的JPA。

### 3-7-1、JPA的产生背景
        ORM（Object Relational Mapping，对象关系映射）是一种用于实现面向对象编程语言里不同类型系统的数据之间转换的程序技术。
    由于面向对象数据库系统（OODBS）的实现在技术上还存在难点，目前市面上流行的数据库还是以关系型数据库为主。
        由于关系型数据库使用的SQL 语言是一种非过程化的面向集合的语言，而目前许多应用仍然是由高级程序设计语言（如Java）来实现的，
    但是高级程序设计语言是过程化的，而且是面向单个数据的，这使得SQL与它之间存在着不匹配，这种不匹配称为“阻抗失配”。
    由于“阻抗失配”的存在，使得开发人员在使用关系型数据库时不得不花很多功夫去完成两种语言之间的相互转化。
        而ORM框架的产生，正是为了简化这种转化操作。在编程语言中，使用ORM就可以使用面向对象的方式来完成数据库的操作。
        ORM框架的出现，使直接存储对象成为可能，它们将对象拆分成SQL语句，从而来操作数据库。但是不同的ORM框架，在使用上存在
    比较大的差异，这也导致开发人员需要学习各种不同的ORM框架，增加了技术学习的成本。
        而JAP规范就是为了解决这个问题:规范ORM框架，使用ORM框架统一的接口和用法。这样在采用面向接口编程的技术中，
    即便更换了不同的ORM框架，也无须变更业务逻辑。
        最早的JPA规范是由Java官方提出的，随Java EE5规范一同发布。
    
    ··实体（Entity )
        实体是轻量级的持久化域对象。通常实体表示关系数据库中的表，并且每个实体实例对应于该表中的行。实体的主要编程工件是实体类，
    尽管实体可以使用辅助类。
        在EJB3之前，EJB主要包含三种类型:会话bean、消息驱动bean、实体bean。但自EJB 3.0开始，实体bean被单独分离出来，
    形成了新的规范：JPA。所以JPA完全可以脱离EJB3来使用。实体是JPA中的核心概念。
        实体的持久状态通过持久化字段或持久化属性来表示。这些字段或属性使用对象/关系映射注解将实体和实体关系映射到基础数据
    存储中的关系数据。
        与实体在概念上比较接近的另外一个领域对象是值对象。实体是可以被跟踪的，通常会有一个主键（唯一标识）来追踪其状态。
    而值对象则没有这种标识，我们只关心值对象的属性。

### 3-7-2、Spring Data JPA概述
        Spring Data JPA是更大的Spring Data家族的一部分，使得轻松实现基于JPA的存储库变得更容易。该模块用于处理对基于JPA的数据访问层
    的增强支持。它使更容易构建基于使用Spring 数据访问技术栈的应用程序。
        Spring Data JPA对于JPA 的支持则是更近一步。使用Spring Data JPA，开发者无须过多关注EntityManager 的创建、事务处理等
    JPA相关的处理，这基本上也是作为一个开发框架而言所能做到的极限了，甚至Spring Data JPA让你连实现持久层业务逻辑的工作都省了，
    唯一要做的，就只是声明持久层的接口，其他都交给Spring Data JPA来帮你完成。
        Spring Data JPA就是这么强大，让你的数据持久层开发工作简化，只需声明一个接口。比如你声明了一个findUserByld()，
    Spring Data JPA就能判断出这是根据给定条件的ID查询出满足条件的User对象，而其中的实现过程开发者无须关心，这一切都交予
    Spring Data JPA来完成。
        对于普通开发者而言，自己实现应用程序的数据访问层是一件极其烦琐的过程。开发者必须编写太多的样板代码来执行简单查询、
    分页和审计。Spring Data JPA旨在通过将努力减少到实际需要的量来显著改进数据访问层的实现。作为开发人员，只需要编写存储库的接口，
    包括自定义查询方法，而这些接口的实现，Spring Data JPA将会自动提供。
        Spring Data JPA包含如下特征。
        ·基于Spring 和JPA来构建复杂的存储库。
        ·支持Querydsl（http://www.querydsl.com）谓词，因此支持类型安全的JPA查询。
        ·域类的透明审计。
        ·具备分页支持、动态查询执行、集成自定义数据访问代码的能力。
        ·在引导时验证带@Query注解的查询。
        ·支持基于XML的实体映射。
        ·通过引人@EnableJpaRepositories来实现基于JavaConfig 的存储库配置。

### 3-7-3、如何使用Spring Data JPA
    在项目中使用spring-data-jpa的推荐方法是使用依赖关系管理系统。下面是使用Gradle构建的示例。
        dependencies {
            compile 'org.springframework.data:spring-data-jpa:2.2.12.RELEASE'
        }
    在代码中，我们只需声明继承自Spring Data JPA中的接口。
        import org.springframework.data.jpa.repository.JpaRepository;
        public interface UserRepository extends JpaRepository<User, Long>(
            List<User> findByNameLike(string name);
        }
        在这个例子中，代码继承自Spring Data JPA中的JpaRepository 接口，而后声明相关的方法即可。比如声明findByNameLike，
    就能自动实现通过名称来模糊查询的方法。

### 3-7-4、Spring Data JPA的核心概念
        Spring Data存储库抽象中的中央接口是Repository。它将域类及域类的ID类型作为类型参数进行管理。此接口主要作为标记接口
    捕获要使用的类型，并帮助发现扩展此接口。而CrudRepository为受管理的实体类提供复杂的CRUD功能。
        public interface CrudRepository<T, ID extends Serializable> extends Repository<T,ID> {
            <S extendsT>S save(s entity);//（1）
            T findone(ID primaryKey);     //（2）
            Iterable<T> findAl1();        //（3）
            Long count();                 //（4）
            void delete(T entity);        //（5）
            boolean exists(ID primaryKey); //（6）
            //省略更多方法...
        } 
        CrudRepository接口中的方法含义如下。
            (1) 保存给定实体。
            (2) 返回由给定ID标识的实体。
            (3) 返回所有实体。
            (4) 返回实体的数量。
            (5) 删除给定的实体。
            (6) 指示是否存在具有给定ID的实体。
        同时还提供其他特定的持久化技术的抽象，比如JpaRepository或MongoRepository，这些接扩展了CrudRepository。
        在CrudRepository的顶部有一个PagingAndSortingRepository抽象，它增加了额外的方法来简化对实体的分页访问。
            public interface PagingAndSortingRepository<T, ID extends Serializab 
                        extends CrudRepository<T, ID> {
                Iterable<T> findAl1(Sort sort);
                Page<T> findAll(Pageable pageable);
            }
        比如，想访问用户的第二页的页面大小为20，可以简单地做这样的事情。
            PagingAndSortingRepository<User,Long> repository =//…获取bean
            Page<User> users = repository. findAll(new PageRequest(1,20));
        除了查询方法外，还可以使用计数和删除查询。
        派生计数查询:
            public interface UserRepository extends CrudRepository<User, Long> {
                Long countByLastname(string lastname);
            } 
        派生删除查询:
            public interface UserRepository extends CrudRepository<User,Long> {
                Long deleteByLastname(String lastname);
                List<User>removeByLastname(String lastname);
            }

### 3-7-5、Spring Data JPA的查询方法
        对于底层数据存储的管理，我们通常使用标准CRUD功能的资源库来实现。使用Spring Data声明这些查询将会变得简单，只需要4步。
    1、声明扩展Repository或其子接口之一的接口
        声明接口，并输入将处理的域类和D类型。
            interface PersonRepository extends Repository<Person, Long> { ... }
    2、在接口上声明查询方法
            interface PersonRepository extends Repository<Person, Long> {
                List<Person> findByLastname(String lastname);
            }
    3、为这些接口创建代理实例
        可以通过JavaConfig的方式：
            interface PersonRepository extends Repository<Person, Long> {
                List<Person> findByLastname (String lastname);
            }
        或通过XML配置方式:
            <? xml version="1.0"encoding="UTF-8"?>
            <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:jpa="http://www.springframework.org/schema/data/jpa"
                xsi:schemaLocation="http://www.springframework.org/schema/beans
                    http://www.springframework.org/schema/beans/spring-beans.xsd
                    http://www.springframework.org/schema/data/jpa
                    http://www.springframework.org/schema/data/jpa/spring-jpa.xsd">
                <jpa:repositories base-package="com.waylau.repositories"/>
            </beans>
        在此示例中使用了JPA命名空间。如果使用任何其他存储库的存储库抽象，则需要将其更改为存储模块的相应命名空间。
        另外，请注意，JavaConfig变量不会明确配置包，因为默认情况下使用注解类的包。如果要自定义扫描的程序包，
    请使用数据存储特定存储库的@Enable注解。例如下面的注解。
        @EnableJpaRepositories(basePackages ="com.waylau.repositories.jpa")
        @EnableMongo Repositories(basePackages ="com.waylau.repositories.mongo")
        interface Configuration { }
    4、获取注入的存储库实例并使用它
        public class someClient {
            @Autowired
            private PersonRepository repository;
            public void doSomething() {
                List<Person> persons = repository.findByLastname("Lau");
            }
        }

## 3-8、实现热插拔（hot swapping）
        对于Java项目而言，在开发过程中，一个非常大的问题在于，每次在修改完文件之后都需要重新编译、启动，才能查看到最新的修改效果，
    这极大影响了开发效率。因此Spring Boot提供了种热插拔（Hot Swapping）方式。

### 3-8-1、重新加载静态内容
        有多种热加载的方式,推荐的方法是使用spring-boot-devtools，因为它提供了额外的功能，例如支持快速应用程序重启和LiveReload
    及智能的开发时配置（如模板缓存）。
        以下是在Maven添加Devtools的方式。
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-devtools</artifactId>
                <optional>true</optional>
            </dependency>
        </dependencies>    
        在Gradle添加Devtools则更加多简洁。
        dependencies {
            compile("org.springframework.boot:spring-boot-devtools")
        }
        Devtools通过监视类路径的变更来实现热加载。这意味着静态资源更改必须构建才能使更改生效。不同的IDE触发更新的方式有所不同。
    默认情况下，在Eclipse中，保存修改的文件将导致类路径被更新并触发重新启动。在IntelliJ IDEA中，构建项目(Build→ Make Project）
    将具有相同的效果。
        在IDE中运行（特别是调试）是另外一个非常好的开发方式，因为几乎所有现代IDE都允许重新加载静态资源，通常还可以热部署Java类的更改。
    -- LiveReload
        spring-boot-devtools模块包括一个嵌人式LiveReload 服务器，可以在资源更改时用于触发浏览器刷新。http://livereload.com/extensions/
    网站为Chrome、Firefox和Safari等免费提供了LiveReload浏览器的扩展程序。
        如果不想在应用程序运行时启动LiveReload服务器，则可以将spring.devtools.livereload.enabled属性设置为false。
        需要注意的是，一次只能运行一个LiveReload服务器。应用程序启动之前，请确保没有其他LiveReload服务器正在运行。
    如果从IDE启动多个应用程序，则只有第一个应用程序将支持LiveReload。

### 3-8-2、重新加载模板
        Spring Boot在大多数模板技术中,都有包括禁用缓存的配置选项。启用这个禁用缓存的选项后修改模板文件，就能自动实现模板的加载。
    如果使用spring-boot-devtools模块，这些属性将在开发时自动配置上。
        下面是常用模板的禁用缓存的设置。
        (1) Thymeleaf
            如果使用Thymeleaf，请设置spring.thymeleaf.cache为false。
        (2) FreeMarker
            如果使用FreeMarker，请设置spring.freemarker.cache为false。
        (3) Groovy
            如果使用Groovy，请设置spring.groovy.cache为falsc。
    
### 3-8-3、应用程序快速重启
        spring-boot-devtools模块支持应用程序自动重新启动。虽然并不像商业软件JRebel那样快，但通常比“冷启动”快得多。
    所以如果不想花费太多资源在这些商业软件身上，不妨尝试下 Devtools。

### 3-8-4、重新加载 Java类而不重新启动容器
        现代IDE(（如Eclipse、IDEA等）都支持字节码的热插拔，所以如果进行了不影响类或方法签名的更改，那么应重新加载Java类，
    而不是重启容器，这样会更快、更干净，而且不会因为重启容器而产生副作用。
