# 第2章 微服务的基石 -- SpringBoot
## 本章知识
    2-1、Spring Boot简介
    2-2、开启第一个Spring Boot项目
    2-3、Hello World项目
    2-4、如何搭建开发环境
    2-5、Gradle 与 Maven 的抉择

## 2-1、Spring Boot简介
### Spring Boot产生的背景
        早期的Java企业级开发规范是JavaEE，但是其重量级的开发核心EJB却由于设计的复杂性，无法找到简单开发并提高生产能力。
    因此Spring应运而生，Spring框架打破了传统EJB开发模式中以bean为重心的强耦合、强侵入性的弊端，采用依赖注入和AOP(面向切面编程）
    等技术，来解耦对象间的依赖关系，无须继承复杂的bean，只需要POJOs（Plain Old Java Objects，简单的Java对象）就能快速
    实现企业级应用的开发。
        Spring框架最初的bean管理是通过XML文件来描述的。然后随着业务的增加，应用里面存在了大量的XML配置，这些配置包括Spring框架
    自身的bean配置,还包括了其他框架的集成配置等，最后XML文件变得臃肿不堪、难以阅读和管理。同时XML文件内容本身不像.class文件一样
    能够在编译期事先做类型校验，所以也就很难排查XML文件中的错误配置。
        正当Spring开发者饱受Spring平台XML配置及依赖管理的复杂性之苦时，Spring团队敏锐地意识到了这个问题。随着Spring3.0的发布，
    SpringIO团队逐渐开始摆脱XML配置文件，并且在开发过程中大量使用“约定大于配置”的思想（大部分情况下就是Java Config的方式）
    来摆脱Spring框架中各类纷繁复杂的配置。
        Spring4.0发布之后，Spring团队抽象出了SpringBoot开发框架。SpringBoot本身并不提供Spring框架的核心特性及扩展功能，
    只是用于快速、敏捷地开发新一代基于Spring框架的应用程序。就是说SpringBoot并不是用来替代Spring的解决方案，而是和Spring框架紧密结合，
    用于提升Spring开发者体验的工具。同时SpringBoot集成大量常用的第三方库的配置，SpringBoot为这些第三方库提供了几乎可以零配置的
    开箱即用的能力。这样大部分的SpringBoot都只需要非常少量的配置代码，使开发者能够更加专注于业务逻辑，而无须进行诸如框架的整合
    等这些只有高级开发者或架构师才能胜任的工作。
        从最根本上讲，SpringBoot就是一些依赖库的集合，它能够被任意项目的构建系统所使用。在追求开发体验的提升方面，SpringBoot
    甚至可以说整个Spring生态系统都使用到了Groovy编程语言。SpringBoot所提供的众多便捷功能，都是借助于Groovy强大的MetaObject协议、
    可插拔的AST转换过程及内置了解决方案引擎所实现的依赖。在其核心的编译模型之中，SpringBoot使用Groovy来构建工程文件，
    所以它可以使用通用的导人和样板方法（如类的main方法）对类所生成的字节码进行装饰(Decorate）。这样使用SpringBoot编写的应用
    能保持非常简洁，却依然可以提供众多的功能。

### 2-1-2、Spring Boot的目标
        简化Java企业级应用是SpringBoot的目标宗旨。SpringBoot简化了基于Spring的应用开发，通过少量的代码就能创建一个独立的、
    产品级别的Spring应用。SpringBoot为Spring平台及第三方库提供开箱即用的设置，这样就可以有条不紊地来进行应用的开发。
    多数SpringBoot应用只需要很少的Spring配置。
        可以使用SpringBoot创建Java应用，并使用java -jar启动它或者也可以采用传统的WAR部署方式。同时SpringBoot也提供了一个运行
    “Spring脚本”的命令行工具。
        Spring Boot主要的目标如下。
        （1）为所有Spring开发提供一个更快更广泛的入门体验。
        （2）开箱即用，不合适时也可以快速抛弃。
        （3）提供一系列大型项目常用的非功能性特征，如嵌入式服务器、安全性、度量、运行状况检查、外部化配置等。
        （4）零配置。无冗余代码生成和XML强制配置，遵循“约定大于配置”。

### 2-1-3、Spring Boot与其他Spring应用的关系
        本质上讲Spring Boot任然是一个Spring应用，但其本身不提供Spring框架的核心特性及扩展功能。
        Spring Boot并不是要成为Spring 平台里面众多“基础层（Foundation )”项目的替代者。 Spring Boot的目标不在于为已解决的问题域
    提供新的解决方案，而是为平台带来另一种开箱即用的开发体验。这种体验从根本上来讲就是简化对Spring已有的技术的使用。
    对于已经熟悉Spring生态系统的开发人员来说，Spring Boot是一个很理想的选择，而对于采用Spring技术的新人来说，Spring Boot
    提供一种更简洁的方式来使用这些技术。

    #1、Spring Boot与Spring框架的关系
        Spring框架通过IoC机制来管理Bean。Spring Boot依赖Spring框架来管理对象的依赖。Spring Boot并不是Spring 的精简版本，
    而是为使用Spring 做好各种产品级准备。Spring Boot本质上仍然是一个Spring应用，只是将各种依赖按照不同的业务需求来进行
    “组装”，成为不同的Starter，如spring-boot-starter-web 提供了快速开发Web应用的框架的集成，spring-boot-starter-data-redis 
    提供了对Redis 的访问。这样开发者无须自行配置不同的类库之间的关系，采用Spring Boot的Starter即可。
   
    #2、Spring Boot与Spring MVC框架的关系
        Spring MVC实现了Web 项目中的MVC模式。如果Spring Boot是一个Web项目的话，可以选择采用Spring MVC来实现MVC模式。
    也可以选择其他类似的框架来实现。

    #3、Spring Boot与 Spring Cloud 框架的关系
        Spring Cloud框架可以实现一整套分布式系统的解决方案（当然其中也包括微服务架构的方案），包括服务注册、服务发现、监控等，
    而Spring Boot只是作为开发单一服务的框架的基础。

### 2-1-4、Starter
        Starter就是用于快速启动Spring应用的“启动器”，其本质是将某些业务功能相关的技术框架进行集成，统一到一组方便的依赖关系
    描述符中，这样开发者就无须关注应用程序依赖配置的细节，大大简化了开启Spring应用的时间。Starter可以说是SpringBoot团队
    为开发人员提供的技术方案的最佳组合，例如想要使用Spring 和JPA进行数据库访问，那就在项目中包含spring-boot-starter-data-jpa依赖即可，
    这对用户来说是极其友好的。所有Spring Boot官方提供的Starter都以spring-boot-starter-*方式来命名，其中*是特定业务功能类型的应用程序。
    这样开发人员就能通过这个命名结构来方便查找自己所需的Starter。
        Spring Boot官方提供的Starter主要分为三类：应用型的Starter、产品级别的Starter、技术型的Starter。

#### 2-1-4-1、应用型Starter
        常用的应用型的Starter包含以下一些种类
    （1）spring-boot-starter：核心Starter包含支持auto-configuration、日志和YAMLo
    （2）spring-boot-starter-activemq：使用Apache ActiveMQ来实现JMS的消息通信。
    （3）spring-boot-starter-amqp：使用Spring AMQP和Rabbit MQ。
    （4）spring-boot-starter-aop：使用Spring AOP和AspectJ来实现AOP功能。
    （5）spring-boot-starter-artemis：使用Apache Artemis来实现JMS的消息通信。
    （6）spring-boot-starter-batch：使用Spring Batch。
    （7）spring-boot-starter-cache：启用Spring框架的缓存功能。
    （8）spring-boot-starter-cloud-connectors：用于简化连接到云平台，如Cloud Foundry和Herokur。
    （9）spring-boot-starter-data-cassandra：使用Cassandra和Spring Data Cassandra。.
    （10）spring-boot-starter-data-cassandra-reactive：使用Cassandra和 Spring Data Cassandra Reactive。
    （11）spring-boot-starter-data-couchbase：使用Couchbase和 Spring Data Couchbaseo
    （12）spring-boot-starter-data-elasticsearch：使用Elasticsearch和 Spring Data Elasticsearcho
    （13）spring-boot-starter-data-jpa：使用基于Hibernate的Spring DataJPA。
    （14）spring-boot-starter-data-ldap：使用Spring Data LDAP。
    （15）spring-boot-starter-data-mongodb：使用MongoDB和Spring Data MongoDBo
    （16）spring-boot-starter-data-mongodb-reactive：使用MongoDB和Spring Data MongoDB Reactive。
    （17）spring-boot-starter-data-neo4j：使用Neo4j和Spring Data Neo4j。
    （18）spring-boot-starter-data-redis：使用Redis和Spring Data Redis，以及Jedis客户端。
    （19）spring-boot-starter-data-redis-reactive：使用Redis和Spring Data Redis Reactive，以及Lettuce客户端。
    （20）spring-boot-starter-data-rest：通过Spring DataREST来呈现Spring Data仓库。
    （21）spring-boot-starter-data-solr：通过Spring Data Solr来使用Apache Solr.
    （22）spring-boot-starter-freemarker：在MVC应用中使用FreeMarker视图。
    （23）spring-boot-starter-groovy-templates：在MVC应用中使用Groovy Templates视图。
    （24）spring-boot-starter-hateoas：使用Spring MVC和Spring HATEOAS来构建基于Hypermedia的RESTful服务应用。
    （25）spring-boot-starter-integration：用于Spring Integration。
    （26）spring-boot-starter-jdbc：使用Tomcat JDBC连接池来使用JDBC。
    （27）spring-boot-starter-jersey：使用JAX-RS和Jersey来构建RESTful服务应用，可以替代 spring-boot-starter-web。
    （28）spring-boot-starter-jooq：使用jOOQ来访问数据库，可以替代spring-boot-starter-data-jpa或spring-boot-starter-jdbc。
    （29）spring-boot-starter-jta-atomikos：使用Atomikos处理JTA事务。
    （30）spring-boot-starter-jta-bitronix：使用Bitronix处理JTA事务。
    （31）spring-boot-starter-jta-narayana：使用Narayana处理JTA事务。
    （32）spring-boot-starter-mail：使用Java Mail和 Spring框架的邮件发送支持。
    （33）spring-boot-starter-mobile：使用Spring Mobile来构建Web应用。
    （34）spring-boot-starter-mustache：使用Mustache视图来构建Web应用。
    （35）spring-boot-starter-quartz：使用Quartz。
    （36）spring-boot-starter-security：使用Spring Security。
    （37）spring-boot-starter-social-facebook：使用Spring Social Facebook.
    （38）spring-boot-starter-social-linkedin：使用Spring Social LinkedIno
    （39）spring-boot-starter-social-twitter:：使用Spring Social Twitter。
    （40）spring-boot-starter-test:：使用JUnit、Hamcrest和 Mockito来进行应用的测试。
    （41）spring-boot-starter-thymeleaf：在MVC应用中使用Thymeleaf视图。
    （42）spring-boot-starter-validation：启用基于Hibernate Validator的Java Bean Validation功能。
    （43）spring-boot-starter-web：使用Spring MVC来构建RESTful Web应用，并使用Tomcat作为默认内嵌容器。
    （44）spring-boot-starter-web-services：使用Spring Web Services。
    （45）spring-boot-starter-webflux：使用Spring框架的Reactive Web支持来构建WebFlux应用。
    （46）spring-boot-starter-websocket：使用Spring框架的WebSocket支持来构建WebSocket应用。

#### 2-1-4-2、产品级别的Starter
    产品级别的Starter的主要有Actuator。
    1）spring-boot-starter-actuator：使用Spring Boot Actuator来提供产品级别的功能，以便帮助开发人员实现应用的监控和管理。

#### 2-1-4-3、技术型的Starter
    Spring Boot还包括一些技术型的Starter，如果要排除或替换特定的技术，可以使用它们。
        1）spring-boot-starter-jetty：使用Jetty作为内嵌容器，可以替换spring-boot-starter-tomcat。
        2）spring-boot-starter-json：用于处理JSON。
        3）spring-boot-starter-log4j2：使用Log4j2来记录日志，可以替换spring-boot-starter-logging
        4）spring-boot-starter-logging：默认采用Logback来记录日志。
        5）spring-boot-starter-reactor-netty：使用Reactor Netty来作为内嵌的响应式的HTTP服务器。
        6）spring-boot-starter-tomcat：默认使用Tomcat作为默认内嵌容器。
        7）spring-boot-starter-undertow：使用Undertow作为内嵌容器，可以替换spring-boot-starter-tomcat。

### 2-1-5、Spring Boot2新特性
    目前Spring Boot团队已经开发到了Spring Boot 2.x版本，本此次学习的所有示例源码基于Spring Boot2.0.0 M4版本来编写的。
    Spring Boot 2相比于Spring Boot 1增加了如下新特性。
        1）对Gradle插件进行了重写。
        2）基于Java8和Spring Framework 5。
        3）支持响应式的编程方式。
        4）对Spring Data、Spring Security、Spring Integration、Spring AMQP、Spring Session、Spring Batch等都做了更新。
  
### 2-1-5-1、Gradle插件
        Spring Boot的 Gradle插件用于支持在Gradle中方便构建Spring Boot应用。它允许开发人员将应用打包成为可执行的jar或war文件，
    运行Spring Boot应用程序，以及管理Spring Boot应用中的依赖关系。Spring Boot2需要Gradle的版本不低于3.4。
        安装 Gradle插件需要添加以下内容。
            buildscript {
                repositories {
                    maven { url 'https://repo.spring.io/libs-milestone'}
                }
                dependencies {
                    classpath'org.springframework.boot:spring-boot-gradle-plugin:2.0.0.M3'
                }
            }
            apply plugin: 'org.springframework.boot'
    
        独立地添加应用插件对项目的改动几乎很少。同时，插件会检测何时应用某些其他插件，并会相应地进行响应。例如当应用Java插件时，
    将自动配置用于构建可执行jar的任务。
        一个典型的Spring Boot项目将至少应用Groovy插件、Java插件或org.jetbrains.kotlin.jvm插件和io.spring.dependency-management插件。
        例如：
            apply plugin: 'java'
            apply plugin: 'io.spring.dependency-management'
        使用Gradle插件来运行Spring Boot应用，只需简单地执行:
            $ ./gradlew bootRun
    
### 2-1-5-2、基于Java8和Spring Framework5
    Spring Boot2基于目前使用范围最稳定的Java8版本和Spring Framework5，这意味着Spring Boot2拥有构建现代应用的能力。
    Java8中的Streams API、Lambda表达式等，都极大地改善了开发体验，让编写并发程序更加容易。
    核心的Spring Framework5.0已经利用Java8所引入的新特性进行了修订。这些内容包括以下几点。
        （1）基于Java8的反射增强，Spring Framework5.0中的方法参数可以更加高效地进行访问。
        （2）核心的 Spring接口提供基于Java8 的默认方法构建的选择性声明。
        （3）支持候选组件索引作为类路径扫描的替代方案。
        （4）最为重要的是，此次Spring Framework5.0推出了新的响应式堆栈Web框架。这个堆栈是完全的响应式且非阻塞，适合于事件
    循环风格的处理，可以进行少量线程的扩展。
    总之，最新的Spring Boot2让开发企业级应用更加简单，可以更加方便地构建响应式编程模型。
    
### 2-1-5-3、Spring Boot周边技术栈的更新
        相应地，Spring Boot2会集成最新的技术栈，包括Spring Data、Spring Security、Spring Integration、Spring AMQP、Spring 
    Session、Spring Batch等都做了更新，其他的第三方依赖也会尝试使用最新的版本，如本次所使用的Spring Data Redis等。
        使用Spring Boot2让开发人员有机会接触最新的技术框架。

## 2-2、开启第一个 Spring Boot项目
### 2-2-1、配置环境
    本次学习例子需要采用如下开发环境。
    （1）JDK 8及以上。
    （2）Gradle 4.0及以上。
    
### 2-2-2、通过Spring Initializr初始化一个Spring Boot原型
        Spring Initializr是用于初始化Spring Boot项目的可视化平台。虽然通过Maven或Gradle来添加Spring Boot 提供的Starter
    使用起来非常简单，但是由于组件和关联部分众多，有这样一个可视化的配置构建管理平台对于用户来说非常友好。
        下面演示如何通过Spring Initializr初始化一个Spring Boot项目原型。  
        访问网站https://start.spring.io/（这是Spring 提供的官方Spring Initializr网站），当然也可以搭建自己的Spring Initializr平台，
    感兴趣的可以访问https://github.com/spring-io/initializr/来获取Spring Initializr项目源码。
        按照Spring Initializr页面提示，选择工程种类输人相应的项目元数据(Project Metadata）资料并选择依赖。由于要初始化一个Web项目，
    在Dependencies项点击“ADD DEPENDENCIES”，并且选择“Spring Web：Build web, including RESTful, applications using Spring MVC.
    Uses Apache Tomcat as the default embedded container”匹项。顾名思义，项目以Spring MVC作为MVC的框架，并且集成了Tomcat作为内嵌的Web容器。
        这里我们采用Gradle作为项目管理工具，Spring Boot版本选型为2.2.12，Group 的信息填为“com.raos.springcloud”，
    Artifact填为“initializr-start”。最后单击“Generate Project”按钮，此时可以下载到以项目“initializr-start”命名的zip包。
    该压缩包包含了这个原型项目的所有源码及配置，将该压缩包解压后，就能获得initializr-start项目的完整源码。
        这里我们并没有输入任何代码，却已经完成了一个完整Spring Boot项目的搭建。

### 2-2-3、用Gradle编译项目
            切换到initializr-start项目的根目录下，执行gradle build来对项目进行构建，构建过程如下。
        1、执行命令：gradle build
        2、执行流程：首先在编译开始阶段，Gradle会解析项目配置文件，而后去 Maven仓库找相关的依赖，并下载到本地。速度快慢
    取决于本地的网络。控制台会打印整个下载、编译、测试的过程，最后显示“BUILD SUCCESSFUL”的字样，说明已经编译成功了。
        回到项目根目录下，会发现多出一个build目录，在该目录build/libs下可以看到一个initializr-start-0.0.1-SNAPSHOT.jar，
    该文件就是项目编译后的可执行文件。在项目的根目录，通过下面的命令来运行该文件。
        java -jar build/libs/initializr-start-0.0.1-SNAPSHOT.jar
        运行成功后可以看到spring标识。该标识也称为Spring Boot的“banner”。
        用户可以根据自己的个性需求来自定义banner。例如类路径下添加一个banner.txt文件，或者将banner.location设置到此类文件的位置来更改。
    如果文件有一个不寻常的编码，也可以设置banner.charset（默认是UTF-8）。除了文本文件，还可以将banner.gif、banner.jpg
    或banner.png图像文件添加到类路径中，或者设置banner.image.location属性。这些图像将被转换成ASCII艺术表现，并打印在控制台上方。
        Spring Boot默认寻找Banner的顺序如下。
        1）依次在类路径下找文件banner.gif、banner.jpg或banner.png，先找到哪个就用哪个。
        2）继续在类路径下找banner.txto
        3）上面都没有找到的话，用默认的Spring Boot Banner，就是在上面控制台输出的最常见的那个。
        从最后的输出内容可以观察到，该项目使用的是Tomcat容器，项目使用的端口号是8080。

### 2-2-4、探索项目
    在启动项目后，在浏览器里面输入“http://localhost:8080/”，我们可以得到如下信息。
        Whitelabel Error Page
        This application has no explicit mapping for/error,so you are seeing
        this as a fallback.
        Tue sep 26 23:52:05 CsT 2017
        There was an unexpected error (type=Not Found,status=404).
        No message available
    由于在我们项目里面，还没有任何对请求的处理程序，所以Spring Boot会返回上述默认的错误提示信息。
        initializr-start项目结构 
        1）build.gradle文件
            在项目的根目录，可以看到build.gradle文件，这个是项目的构建脚本。Gradle是以Groovy语言为基础，面向Java应用为主。
        基于DSL（领域特定语言）语法的自动化构建工具。Gradle这个工具集成了构建、测试、发布及常用的其他功能，如软件打包、
        生成注释文档等。与以往Maven等构架工具不同，配置文件不需要烦琐的XML，而是简洁的Groovy语言脚本。
            对于本项目的build.gradle文件中配置的含义，下面已经添加了详细注释。
                //使用插件
                plugins {
                	id 'org.springframework.boot' version '2.2.12.RELEASE'
                	id 'io.spring.dependency-management' version '1.0.10.RELEASE'
                	id 'java'
                }
                //项目名、版本、jdk编译版本，默认jar包
                group = 'com.raos.springcloud'
                version = '0.0.1-SNAPSHOT'
                sourceCompatibility = '1.8'
                //使用maven中央仓库
                repositories {
                	mavenCentral()
                }
                //依赖
                dependencies {
                	implementation 'org.springframework.boot:spring-boot-starter-web'
                	testImplementation('org.springframework.boot:spring-boot-starter-test') {
                		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
                	}
                }
                //测试
                test {
                	useJUnitPlatform()
                }

        2）gradlew和gradlew.bat文件
            gradlew和gradlew.bat这两个文件是Gradle Wrapper用于构建项目的脚本。使用Gradle Wrapper的好处在于使项目组成员
        不必预先在本地安装好Gradle工具。在用Gradle Wrapper构建项目时，Gradle Wrapper首先会去检查本地是否存在Gradle，
        如果没有会根据配置上的Gradle的版本和安装包的位置来自动获取安装包并构建项目。使用Gradle Wrapper的另一个好处在于，
        所有的项目组成员能够统一项目所使用的Gradle版本，从而规避由于环境不一致导致的编译失败的问题。对于Gradle Wrapper的使用，
        在类似UNIX的平台上(如Linux和Mac OS)，直接运行gradlew脚本，就会自动完成Gradle环境的搭建。而在Windows环境下，
        则执行gradlew.bat文件。
        2）build和.gradle目录
            build和.gradle目录都是在Gradle对项目进行构建后生成的目录和文件。
        4.Gradle Wrapper
            Gradle Wrapper免去用户在使用Gradle进行项目构建时需要安装Gradle的烦琐步骤。每个Gradle Wrapper都绑定到一个特定版本
        的Gradle，所以当第一次在给定Gradle版本下运行上面的命令之一时，它将下载相应的Gradle发布包，并使用它来执行构建。
        默认情况下，Gradle Wrapper的发布包指向的是官网的Web服务地址，相关配置记录在gradle-wrapper.properties文件中。
        我们查看一下Spring Boot 提供的这个Gradle Wrapper的配置，参数“distributionUrl”就是用于指定发布包的位置
            distributionBase=GRADLE_USER_HOME
            distributionPath=wrapper/dists
            distributionUrl=https\://services.gradle.org/distributions/gradle-6.7.1-bin.zip
            zipStoreBase=GRADLE_USER_HOME
            zipStorePath=wrapper/dists
            从上述配置可以看出，当前Spring Boot采用的是Gradle 4.0.2版本。我们也可以自行来修改版本和发布包存放的位置。
        例如下面这个例子，我们指定了发布包的位置在本地的文件系统中。
            distributionUrl=file\:/D:/software/webdev/java/gradle-6.7.1-all.zip
        5）src目录
            如果用过Maven，那么肯定对src目录不陌生。Gradle约定了该目录下的main目录下是程序的源码，test下是测试用的代码。

### 2-2-5、如何提升Gradle的构建速度
        由于Gradle工具是舶来品，所以对于国人来说，很多时候会觉得编译速度非常慢。这里面很大一部分原因是由于网络的限制，
    毕竟Gradle及Maven的中央仓库都架设在国外，国内要访问速度上肯定会有一些限制。下面介绍几个配置技巧来提升Gradle 的构建速度。
    1）Gradle Wrapper指定本地
        Gradle Wrapper是为了便于统一版本。如果项目组成员都明确了GradleWrapper，尽可能事先将Gradle放置到本地，
    而后修改Gradle Wrapper配置，将参数“distributionUrI”指向本地文件。例如，将Gradle放置到D盘的某个目录:
        # distributionUrl=https\://services.gradle.org/distributions/gradle-4.0.2-bin.zip
        distributionUrl=file\:/D:/software/webdev/java/gradle-4.0-all.zip
    2）使用国内Maven镜像仓库
        Gradle可以使用Maven镜像仓库。使用国内的Maven镜像仓库可以极大地提升依赖包的下载速度。下面演示了使用自定义镜像的方法。
            repositories {
                //mavenCentral()
                maven{ url "https://repo.spring.io/snapshot"}
                maven{ url "https://repo.spring.io/milestone" }
                maven{ url "http://maven.aliyun.com/nexus/content/groups/public/"}
            }
        注释掉了下载缓慢的中央仓库，改用自定义的镜像仓库。

## 2-3、Hello World项目
        依照编程的惯例，第一个简单的程序都是从编写“Hello World”开始。本节也依照惯例来实现一个Spring Boot版本的“Hello World”应用。
    该“Hello World”应用可以基于上一节中实现的initializr-start项目做少量的调整。“Hello World"应用是我们将要编写的一个
    最简单的Web项目。当我们访问项目时，界面会打印出“Hello World"的字样。

### 2-3-1、编写项目构建信息
        我们创建一个新的hello-world目录，并复制在上一节用到的样例程序initializr-start的源码，到新的hello-world目录下，
    当然相关的编译文件(如 build、.gradle等目录下的文件)就不需要复制了。最终新项目的根目录下会有gradle、src目录及build.gradle、
    gradlew.bat、gradlew。在该项目上做一点小变更，就能生成一个新项目的构建信息。
        打开build.gradle文件,做一下修改变更。默认情况下SpringBoot的版本都是0.0.1-SNAPSHOT，这里改为1.0.0，意味着是一个成熟
    的可用的项目。
        version- '1.0.0'，
        dependencies下添加：testCompile group: 'junit', name: 'junit', version: '4.12'
    在项目构建成功之后，可以在build/libs/目录下看到一个名为hello-world-1.0.0.jar的可执行文件。
    项目导入idea的方法
        1）新建工程导入：打开Intellij IDEA，点击Import Project，选择本地的gradle项目，选择Gradle，点击 Next按钮，
            Gradle home修改成本地安装gradle的路径并且选中，如：D:/Program Files/gradle/gradle-5.6，点击Finish即可。
        2）已有工程空间导入：点击Flie ->New -> Module from Existing Sources，选中放置到工程空间中的gradle项目，然后参照1的配置设置。

### 2-3-2、编写程序代码
        现在终于可以进入编写代码的时间了。我们进入hello-world项目的src目录下，应该能够看到项目包名及InitializrStartApplication.java文件。
    为了规范，需将该包名改为包名后添加.weather，将InitializrStartApplication.java更名为Application.java。
        更名最好是在Java IDE(Integrated Development Environment，集成开发环境)中进行。这样借助IDE的“重构”功能快速实现改名。
    
    1、观察Application.java
        Application.java代码核心如下：
            @SpringBootApplication
            public class Application {
            	public static void main(String[] args) {
            		SpringApplication.run(Application.class, args);
            	}
            }
        
        首先看到的是@SpringBootApplication注解。对于经常使用Spring 的用户而言，很多开发者总是使用@Configuration、@EnableAutoConfiguration
    和@ComponentScan注解main类。由于这些注解被频繁地一起使用，于是Spring Boot 提供@SpringBootApplication选择，该@SpringBootApplication
    注解的默认属性，等同于使用@Configuration、@EnableAutoConfiguration和@ComponentScan三个注解组合的默认属性。即：
    @SpringBootApplication =（默认属性的)@Configuration + @EnableAutoConfiguration + @ComponentScan。
        由于springboot从2.0开始有了属于自己的@SpringBootConfiguration，因此@Configuration就被隐藏在弄@SpringBootConfiguration，
    故可以看出@Configuration注解还是在@SpringBootApplication中。
        
        它们的含义分别如下。
            1）@Configuration：经常与@Bean组合使用，使用这两个注解就可以创建一个简单的Spring配置类，用来替代相应的XML配置文件。
        @Configuration的注解类标识这个类可以使用Spring IOC容器作为bean定义的来源。@Bean注解告诉Spring，一个带有@Bean 的注解方法
        将返回一个对象，该对象应该被注册为在Spring应用程序上下文中的bean。
            2）@EnableAutoConfiguration：能够自动配置Spring的上下文，试图猜测和配置想要的bean类，通常会自动根据类路径和Bean定义自动配置。
            3）@ComponentScan：会自动扫描指定包下的全部标有@Component的类并注册成bean，当然包括@Component下的子注解@Service、
        @Repository、@Controller。这些bean一般是结合@Autowired构造函数来注入。
    
        所以，不要小看这么一个小小的注解，其实它融汇了很多Spring里面的概念，包括自动扫描包、自动装配bean、声明式配置等，
    这些都有利于最大化减少项目的配置，采用约定的方式来实现bean的依赖注入。
        按照约定，声明了@SpringBootApplication注解的Application类，应处于项目的根目录下，这样才能让Spring正确扫描包，
    实现bean的正确注入。

    2、main方法
        该Application类的 main 方法是一个标准的 Java 方法，它遵循 Java 对一个应用程序入口点的约定。main方法通过调用 run ，
    将业务委托给了Spring Boot 的SpringApplication类。SpringApplication将引导应用启动Spring，相应地启动被自动配置的Tomcat Web服务器。
    因此将Application.class作为参数传递给run方法，以此告诉SpringApplication哪个是主要的Spring组件，并传递args数组以暴露所有的命令行参数。

    3、编写控制器HelloController
        HelloController.java的代码非常简单。当请求到/hello路径时，将会响应“Hello World!”字样的字符串给浏览器。代码如下。
            package com.raos.springcloud.weather.controller;
            import org.springframework.web.bind.annotation.RequestMapping;
            import org.springframework.web.bind.annotation.RestController;
            @RestController
            public class HelloController {
                @RequestMapping("/hello")
                public String hello() {
                    return "Hello World! Welcome to visit Gradle, Spring Boot!";
                }
            }
        其中，@RestController等价于@Controller与@ResponseBody的组合，主要用于返回在RESTful应用常用的JSON格式数据。即:
    @RestController = @Controller + @ResponseBody
        其中:
            1）@ResponseBody：该注解指示方法的返回值应绑定到Web响应正文;
            2）@RequestMapping：是一个用来处理请求地址映射的注解，可用于类或方法上。
                用于类上，表示类中的所有响应请求的方法都是以该地址作为父路径。根据方法的不同，还可以用GetMapping、PostMapping、
                PutMapping、DeleteMapping、PatchMapping 代替;
            3）@RestController：暗示用户，这是一个支持REST的控制器。

### 2-3-3、编写测试用例代码
        在test目录下，项目包主路径更正为 com.raos.springcloud.weather，测试主类命名为ApplicationTests.java
    1）编写HelloControllerTest.java 测试类
        测试类包名对于源程序，测试类HelloControllerTest.java代码如示：
            package com.raos.springcloud.weather.controller;
            
            import org.junit.Test;
            import org.junit.runner.RunWith;
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
            import org.springframework.boot.test.context.SpringBootTest;
            import org.springframework.http.MediaType;
            import org.springframework.test.context.junit4.SpringRunner;
            import org.springframework.test.web.servlet.MockMvc;
            import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
            import static org.hamcrest.Matchers.equalTo;
            import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
            import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
            
            @RunWith(SpringRunner.class)
            @SpringBootTest
            @AutoConfigureMockMvc
            public class HelloControllerTest {
                @Autowired
                private MockMvc mockMvc;
                @Test
                public void testHello() throws Exception {
                    mockMvc.perform(MockMvcRequestBuilders.get("/hello").accept(MediaType.APPLICATION_JSON))
                            .andExpect(status().isOk())
                            .andExpect(content().string(equalTo("Hello World! Welcome to visit Gradle, Spring Boot!")));
                }
            }
            
    2）运行测试类（idea中）
        基于JUnit运行测试，绿色代表测试通过。
 
### 2-3-4、配置Gradle Wrapper       
        Gradle项目可以使用Gradle的安装包进行构建，也可以使用Gradle Wrapper来进行构建。使用Gradle Wrapper的好处是可以使
    项目的构建工具版本得到统一。我们修改Wrapper属性文件(位于gradle/wrapper/gradle-wrapper.properties）中的 distributionUrl属性，
    将其改为指定的Gradle版本，这里是采用了Gradle5.6版本：distributionUrl=https\://services.gradle.org/distributions/gradle-5.6-bin.zip 
    或者指向本地的文件：distributionUrl=file\:/D:/software/webdev/java/gradle-5.6-bin.zip
        这样，Gradle Wrapper会自动安装Gradle的版本。
        不同平台执行不同的命令脚本。gradlew (UNIX Shell脚本）；gradlew.bat ( Windows批处理文件）。

### 2-3-5、运行程序
    1、使用Gradle Wrapper
        执行gradlew 来对“hello-world”程序进行构建。
        命令：gradlew build
        如果是首次使用，会下载Gradle发布包。可以系统盘下的$USER_HOME/.gradle/wrapper/dists下的用户主目录中找到。
    
    2、运行程序
        执行命令 java -jar build/libs/hello-world-0.0.1-SNAPSHOT.jar 来运行程序
        
    3、访问程序
        浏览器输入：http://localhost:8080/hello。
    
### 2-3-6、其他运行程序的方式
    有多种运行Spring Boot程序的方式，除了上面介绍的使用java -jar命令外,还有以下几种方式。
        1、以“Java Application”运行
            hello-world应用就是一个平常的Java程序，所以可以直接在IDE里面右击项目，以“Java Application”方式来运行程序。这种方式在开发时，非常方便调试程序。
        2、使用Spring Boot Gradle Plugin插件运行
            Spring Boot已经内嵌了Spring Boot Gradle Plugin插件，所以可以使用Spring Boot Gradle Plugin插件来运行程序。
            在命令行执行方式如下。
                $ gradle bootRun 或者 $gradlew bootRun       
    
## 2-4、如何搭建开发环境
### 2-4-1、JDK安装
    1、下载安装JDK
    2、设置环境变量
    3、验证jdk是否安装完成

### 2-4-2、Gradle安装
    1、前置条件
        Gradle需要JavaJDK或JRE，版本是7及以上。Gradle将会装载自己的Groovy 库，因此Groovy不需要被安装。任何存在的Groovy安装都会被Gradle忽略。
    Gradle将会使用任何在路径中找到的JDK，或者可以设置“JAVA_HOME”环境变量来指向所需的JDK安装目录。
    2、下载
        可以从官网https://www.gradle.org/downloads位置来安装Gradle的发布包。
    3、解压
        Gradle的发布包被打包成ZIP。完整的发布包含:
        1）Gradle二进制；
        2）用户指南（HTML和PDF)；
        3）DSL参考指南；
        4）API文档（Javadoc和Groovydoc）
        5）扩展示例，包括用户指南中引用的例子以及一些完整的和更复杂的构建可以作为自己开始的构建；
        6）二进制源文件。
    4、环境变量
        设置“GRADLE_HOME”环境变量指向Gradle的解压包，并添加“GRADLE_HOME/bin”到“PATH”环境变量。
    5、运行和测试安装
        通过gradle命令运行Gradle。gradle -v 用来查看安装是否成功。
    6、虚拟机选项
        虚拟机选项可以设置Gradle的运行环境变量。可以使用GRADLE_OPTS或JAVA_OPTS，或者两个都选。JAVA_OPTS约定和Java共享环境变量。
    典型的案例是在JAVA_OPTS 设置HTTP代理，在GRADLE_OPTS设置内存。这些变量也可在gradle或gradlew脚本开始时设置。

### 2-4-3、项目导入IDEA
        下面将介绍如何来安装和配置IDE，并将Spring Boot项目导入IDE中进行开发。本例选用的IDE为IDEA，也可以是其他的。
    例子中的源码是与具体IDE无关的。
        一款好用的IDE就如同一件称手的兵器，挥舞起来自然得心应手。好用的IDE可以帮助用户：
        1）提升编码效率。大部分IDE都提供了代码提示、代码自动补全等功能，极大地提升了编码的效率。
        2）纠错。在编码过程中，IDE也可以对一些运行时、编译时的常见错误做出提示。
        3）养成好的编码规范。IDE可以对代码格式做校验，这样无形中就帮助用户来纠正错误的编码习惯。
    1、导入项目到IDEA
        1）新建工程导入：打开Intellij IDEA，点击Import Project，选择本地的gradle项目，选择Gradle，点击 Next按钮，
            Gradle home修改成本地安装gradle的路径并且选中，如：D:/Program Files/gradle/gradle-5.6，点击Finish即可。
        2）已有工程空间导入：点击Flie ->New -> Module from Existing Sources，选中放置到工程空间中的gradle项目，
            然后参照1的配置设置。

## 2-5、Gradle 与 Maven 的抉择
### 2-5-1、Maven概述
        Maven主要实现Java程序的编译以及依赖管理。但Maven不仅内置依赖管理，更有一个可能拥有全世界最多Java开源软件包的中央仓库，
    Maven用户无须进行任何配置，就可以直接享用。除此之外，企业也可以在自己的网络中搭建Maven镜像库，从而加快下载依赖的速度。
    1、Maven生命周期
        Maven主要有以下三种内建的生命周期。
        1）default：用于处理项目的构建和部署。
        2）clean：用于清理Maven产生的文件和文件夹。
        3）site：用于处理项目文档的创建。
        在实际使用中，我们无须明确指定生命周期。相反只需要指定一个阶段。Maven 会根据指定的阶段来推测生命周期。
    
    2、依赖管理
        依赖管理是Maven的核心功能。Maven为Java世界引人了一个新的依赖管理系统。在Java 世界中，可以用groupld、artifactld、
    version组成的Coordination(坐标）唯一标识一个依赖。任何基于Maven构建的项目自身也必须定义这三项属性，生成的包可以是jar包，
    也可以是war包或ear包。以下是一个典型的Maven依赖库的坐标。
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            version>2.2.12.RELEASE</version>
        </dependency>
        在依赖管理中，另一个非常重要的概念是 scope（范围）。Maven有6种不同的scope。
        1）compile：默认就是compile，什么都不配置也就是意味着compile。compile表示被依赖项目需要参与当前项目的编译，
            当然后续的测试，运行周期也参与其中，是一个比较强的依赖。打包的时候通常需要包含进去。
        2）test：此类依赖项目仅仅参与测试相关的工作，包括测试代码的编译和执行。一般在运行时不需要这种类型的依赖。
        3）runtime：表示被依赖项目无须参与项目的编译，不过后期的测试和运行周期需要其参与。一个典型的例子是logback，
            你希望使用Simple Logging Facade for Java ( slf4j）来记录日志，并使用logback绑定。
        4）provided：该类依赖只参与编译和运行时，但并不需要在发布时打包进发布包。一个典型的例子是servlet-api，
            这类依赖通常会由应用服务来提供。
        5）system：从参与度来说，与provided相同，不过被依赖项不会从Maven仓库获取，而是从本地文件系统获取，
            所以一定需要配合systemPath属性使用。以下是一个例子。
            <dependency>
                <groupId>com.raos.spring</groupId>
                <artifactId>boot</artifactId>
                <version>2.0</version>
                <scope>system</scope>
                <systemPath>${basedir}/lib/boot.jar</systemPath>
            </dependency>
        6）import：这仅用于依赖关系管理部分中 pom类型的依赖。它表示指定的pom应该被替换为该pom的dependencyManagement部分中的依赖关系。
            这是为了集中大型多模块项目的依赖关系。

    3、多模块构建
        Maven支持多模块构建。在现代的项目中，经常需要将一个大型软件产品划分为多个模块来进行开发，从而实现软件项目的“高内聚、低耦合”。
        Maven的多模块构建是通过一个名为项目继承(Project Inheritance）的功能来实现的。Maven允许将一些需要继承的元素，在父pom文件中进行指定。
        一般来说，多模块项目包含一个父模块，以及多个子模块。下面是一个父模块的pom文件的例子。
            <groupId>com.raos.spring</groupId>
            <artifactId>project-with-inheritance</artifactId>
            <packagingpom</packaging>
            <version>1.0.0</version>
        那么，在子模块的pom中需要指定父模块。
        <parent>
            <groupld>com.raos.spring</groupId>
            <artifactId>project-with-inheritance</artifactId>
            <version>1.0.0</version>
        </parent>
        <modelVersion>4.0.0</modelVersion>
        <artifactId>child</artifactId>
        <packaging>jar</packaging>
        <name>Child Project</name>

### 2-5-2、Gradle概述
        Gradle是一个基于Ant 和Maven概念的项目自动化构建工具。与Ant和Maven最大的不同之处在于，它使用一种基于Groovy 的特定领域语言(DSL）
    来声明项目设置，抛弃了传统的基于XML的各种烦琐配置。
    1、Gradle生命周期
        Gradle是基于编程语言的，可以自己定义任务( task)和任务之间的依赖，Gradle会确保有顺序地去执行这些任务及依赖任务，
    并且每个任务只执行一次。当任务执行的时候，Gradle要完成任务和任务之间的定向非循环图(Directed Acyclic Graph)。
        Gradle构建主要有三个不同的阶段。
        1）初始化阶段(Initialization )：Gradle支持单个和多个项目的构建。Gradle在初始化阶段决定哪些项目( project）参与构建，
            并且为每个项目创建一个Project类的实例对象。
        2）配置阶段(Configuration)：在这个阶段配置每个Project的实例对象。然后执行这些项目脚本中的一部分任务。
        3）执行阶段(Execution )：Gradle确定任务的子集，在配置界面创建和配置这些任务，然后执行任务。
            这些子集任务的名称当成参数传递给gradle命令和当前目录。接着Gradle执行每一个选择的任务。

    2、依赖管理
        通常一个项目的依赖会包含自己的依赖。例如Spring 的核心需要几个其他包在类路径中存在才能运行。所以当Gradle运行项目的测试，
    它也需要找到这些依赖关系，使它们存在于项目中。Gradle借鉴了Maven里面依赖管理很多的优点，甚至可以重用Maven中央库。
    你也可以将自己的项目上传到一个远程Maven库中。这也是Gradle能够成功的非常重要的原因─站在巨人的肩膀之上，而非重复发明轮子。
        
    下面是Gradle声明依赖的例子。
        apply plugin:'java'
        repositories {
            mavenCentral()
        }
        dependencies {
            compile group: 'org.hibernate', name: 'hibernate-core', version: '3.6.7.Final'
            testCompile group: 'junit'， name: 'junit', version: '4.+'
        }
        这个脚本说明了几个问题。首先声明使用了Java插件。其次项目需要Hibernate core 3.6.7.Final版本来编译。
    其中隐含的意思是，Hibernate core和它的依赖在运行时是需要的。最后需要junit >= 4.0版本在测试时编译。
    同时告诉Gradle依赖要在Maven中央库中寻找。

    3、多模块构建
        Gradle天然地支持多项目构建。
        以下是一个多项目构建的例子。在该例子中有一个父项目，以及两个Web应用程序的子项目。整个构建布局如下。
        webDist/
            settings.gradle
            build.gradle
            date/
                src/main/java/
                    org/gradle/sample/
                        Dateservlet.java
            hello/
                src/main/java/
                    org/gradle/sample/
                        HelloServlet.java
        其中，settings.gradle文件包含如下内容。
        include 'date', 'hello'
        build.gradle文件包含如下内容。
        allprojects {
            apply plugin: 'java'
            group ='org.gradle.sample'
            version= '1.0'
        }
        subprojects {
            apply plugin:'war'
            repositories {
                mavenCentral()
            }
            dependencies{
                compile "javax.servlet:servlet-api:2.5"
            }
        }
        task explodedDist(type: Copy) {
            into "$buildDir/explodedDist"
            subprojects {
                from tasks.withType(War)
            }
        }
            
### 2-5-3、Gradle 与Maven的对比
        Gradle号称是下一代的构建工具，吸取了Maven等构建工具的优势，所以在一开始的设计上，就比较前瞻。
    从上面的Gradle和Maven概述中，我们也能大概了解到这两个构建工具的异同点。
    
    1、一致的项目结构
        对于源码而言，Gradle与Maven拥有一致的项目结构，以下是项目结构的例子。
            -src
                -main
                    -java
                        -com
                            -raos
                                -springcloud
                                    -initializrstart
                                        InitializrStartApplication.java
                    -resources
                        application.properties
                        banner-jpg
                        -static
                        -templates
                -test
                    -java
                        -com
                            -raos
                                -springcloud
                                    -initializrstart
                                        InitializrStartApplicationTests.java
        Gradle与 Maven同样都遵循“约定大于配置”的原则，以最大化减少项目的配置。
    
    2、一致的仓库
        Gradle借鉴了Maven的坐标表示法，都可以用groupld、artifactld、version组成的坐标来唯一标识一个依赖。
    在类库的托管方面，Gradle并没有自己去创建独立的类库托管平台，而是可以直接使用Maven托管类库的仓库。
    下面是一个在Gradle中指定托管仓库的例子。
        //使用了Maven的中央仓库及Spring自己的仓库（也可以指定其他仓库）
        repositories {
            mavenCentral ()
            maven{ url "https://repo.spring.io/snapshot"〕
            maven { url "https://repo.spring.io/milestone" }
        }
    
    3、支持大型软件的构建
        对于大型软件构建的支持，Maven采用了多模块的概念，而Gradle采用了多项目的概念，两者本质上都是为了简化大型软件的开发。
    
    4、丰富的插件机制
        Gradle和Maven都支持插件机制，而且社区对于这两款构建工具的插件的支持都非常丰富。
    
    5、Groovy而非XML
        在依赖管理的配置方面，Gradle采用了Groovy语言来描述，而非传统的XML。XML的好处是语言严谨，这也是为什么在Web服务中
    采用XML来作为信息交换的格式。但这同样也带来了一个弊端，那就是灵活度不够。而Groovy本身是一门编程语言，所以在灵活性方面更胜一筹。
    另—方面，Groovy在表达依赖关系时，比XML拥有更加简洁的表示方式。例如，下面在Maven中引用Spring Boot依赖的例子。
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starters</artifactId>
            <version>2.2.12.RELEASE</version>
        </dependency>
        如果换作是Gradle，仅仅只需一行配置。可以改用以下的方式。
        compile('org.springframework.boot:spring-boot-starter-web:2.2.12.RELEASE')
        从这个小小的例子就能看出，XML对于Gradle的配置脚本而言，是多么低效和冗余!
    
    6、性能对比
        相比于Maven而言，Gradle的性能可以说是一大亮点。根据官方公布的对比性能，构建等同项目Gradle耗时远小于Maven。
        
### 2-5-4、总结
        我们已经对Gradle与Maven做了优势比较。Maven在Java领域仍然拥有非常高的占有率，但在将来，越来越多的团队已经开始转向了Gradle，
    如Linkin、Android Studio、Netflix、Adobe、Elasticsearch等，毕竟无论是在配置的简洁性方面，还是在性能方面，Gradle都更胜一筹。
    这也是为什么接下来的案例选用Gradle来作为构建工具。
    
        注意:本次学习所有案例，都是采用Gradle来进行项目的管理。如有需要也可以将项目源码自行转化为Maven等管理方式。
    
    