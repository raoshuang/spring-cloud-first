# 第5章 微服务的协调者--Spring Cloud
## 本章内容
    5-1、Spring Cloud 简介
    5-2、Spring Cloud 入门配置
    5-3、Spring Cloud 的子项目介绍

## 5-1、Spring Cloud 简介
    从零开始构建一套完整的分布式系统是困难的。在1.2节中，我们讨论众多的分布式系统的架构，可以说每种架构都有其优势及局限，
    采用何种架构风格要看应用程序当前的使用场景。就微服务架构的风格而言，一套完整的微服务架构系统往往需要考虑以下挑战。
        ·配置管理。
        ·服务注册与发现。
        ·断路器。
        ·智能路由。
        ·服务间调用。
        ·负载均衡。
        ·微代理。
        ·控制总线。
        ·一次性令牌。
        ·全局锁。
        ·领导选举。
        ·分布式会话。
        ·集群状态。
        ·分布式消息。
        ... ...
    而iSpring Cloud正是考虑到上述微服务开发过程中的痛点，为广大的开发人员提供了快速构建微服务架构系统的工具。

### 5-1-1、什么是Spring Cloud
        使用Spring Cloud，开发人员可以开箱即用地实现这些模式的服务和应用程序。这些服务可以在任何环境下运行，包括分布式环境，
    也包括开发人员自己的笔记本电脑、裸机数据中心，以及Cloud Foundry 等托管平台。
        Spring Cloud基于Spring Boot来进行构建服务，并可以轻松地集成第三方类库，来增强应用程序的行为。您可以利用基本的默认行为
    快速人门，然后在需要时，通过配置或扩展以创建自定义的解决方案。
        Spring Cloud的项目主页为http://projects.spring.io/spring-cloud/。

### 5-1-2、Spring Cloud与Spring Boot的关系
        Spring Boot是构建Spring Cloud架构的基石，是一种快速启动项目的方式。
        Spring Cloud 的版本命名方式与传统的版本命名方式稍有不同。由于Spring Cloud是一个拥有诸多子项目的大型综合项目，
    原则上其子项目也都维护着自己的发布版本号。那么每一个Spring Cloud的版本都会包含不同的子项目版本，为了管理每个版本的
    子项目清单，避免版本名与子项目的发布号混淆，所以没有采用版本号的方式，而是通过命名的方式。
        这些版本名字采用了伦敦地铁站的名字，根据字母表的顺序来对应版本的时间顺序，比如最早的Release版本为Angel，
    第二个Release版本为Brixton，以此类推。Spring Cloud对应于Spring Boot版本，有以下的版本依赖关系。
        ·Finchley版本基于Spring Boot 2.0.x，不能工作于Spring Boot 1.5.x。
        ·Dalston和Edgware基于Spring Boot 1.5.x，但不能工作于Spring Boot 2.0.x。
        ·Camden工作于Spring Boot 1.4.x，但未在1.5.x版本上测试。
        ·Brixton工作于Spring Boot 1.3.x，但未在1.4.x版本上测试。
        ·Angel基于Spring Boot 1.2.x，且不与Spring Boot 1.3.x版本兼容。
        以下所有的案例，都基于Spring Cloud Finchley.M2版本来构建，与之相兼容的Spring Boot版本为2.0.0.M3。

## 5-2、Spring Cloud 入门配置
    在项目中开始使用Spring Cloud 的推荐方法是使用依赖关系管理系统，例如，使用Maven或Gradle构建。
    
### 5-2-1、Maven依赖配置
    以下是一个Spring Boot项目的基本 Maven 配置。
        <parent>
            <groupId>org.springframework.boot</groupld>
            <artifactId>spring-boot-starter-parent</artifactId>
            <version>2.0.0.M3</version>
        </parent>
        <dependencyManagement>
            <dependencies>
                <dependency>
                    <groupId>org.springframework.cloud</groupId>
                    <artifactId>spring-cloud-dependencies</artifactId>
                    <version>Finchley.M2</version>
                    <type>pom</type>
                    <scope>import</scope>
                </dependency>
            </dependencies>
        </dependencyManagement>
        <dependencies>
            <dependency>
                <groupId></groupId>
                <artifactId>spring-cloud-starter-eureka</artifactId>
            </dependency>
        </dependencies><repositories>
            <repository>
                <id>spring-milestones</id>
                <name>Spring Milestones</name>
                <url>https://repo.spring.io/libs-milestone</url>
                <snapshots>
                    <enabled>false</enabled>
                </snapshots>
            </repository>
        </repositories>
    在此基础之上，可以按需添加不同的依赖，以使应用程序增强功能。

### 5-2-2、Gradle配置
    以下是一个Spring Boot项目的基本Gradle配置。
        buildscript {
            ext {
                springBootVersion ='2.0.0.M3'
            }    
            repositories {
                mavenCentral()
                maven { url "https://repo.spring.io/snapshot"}
                maven { url "https://repo.spring.io/milestone"}
            }
            dependencies {
                classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
            }
        }
        apply plugin: 'java'
        apply plugin: 'org.springframework.boot'
        apply plugin: 'io.spring.dependency-management'
        
        version = '1.0．0'
        sourceCompatibility = 1.8
        
        repositories {
            mavenCentral()
            maven { url "https://repo.spring.io/snapshot" }
            maven { url "https://repo.spring.io/milestone" }
        }
        ext{
            springCloudVersion = 'Finchley.M2'
        dependencies{
            compile('org.springframework.cloud:spring-cloud-starter-netflix-eureka-client')
            testCompile('org.springframework.boot:spring-boot-starter-test')
        }
        dependencyManagement {
            imports {
                mavenBom "org.springframework.cloud:spring-cloud-dependencies:${springCloudVersion}"
            }
        }
    在此基础之上，可以按需添加不同的依赖，以使应用程序增强功能。
    其中，关于Maven仓库设置，我们可以更改为国内的镜像库，以提升下载依赖的速度。

### 5-2-3、申明式方法
    Spring Cloud采用声明的方法，通常只需要一个类路径更改或添加注解，即可获得很多功能。下面是Spring Cloud声明为一个 
    Netflix Eureka Client最简单的应用程序示例。
        @SpringBootApplication
        @EnableDiscoveryClient
        public class Application {
            public static void main (string[] args) {
                SpringApplication.run(Application.class, args);
            }
        }

## 5-3、Spring Cloud 的子项目介绍
    本节将介绍 Spring Cloud 子项目的组成，以及它们之间的版本对应关系。
### 5-3-1、Spring Cloud 子项目的组成
    Spring Cloud由以下子项目组成。
        ·Spring Cloud Config。
            配置中心——利用git来集中管理程序的配置。
            项目地址为:http://cloud.spring.io/spring-cloud-config。
        ·Spring Cloud Netflix。
            集成众多Netflix的开源软件，包括Eureka、Hystrix、Zuul、Archaius等等。
            项目地址为: http://cloud.spring.io/spring-cloud-netflix。
        ·Spring Cloud Bus。
            消息总线—─利用分布式消息将服务和服务实例连接在一起，用于在一个集群中传播状态的变化，比如配置更改的事件。
            可与Spring Cloud Config联合实现热部署。
            项目地址为: http://cloud.spring.io/spring-cloud-bus。
        ·Spring Cloud for Cloud Foundry。
            利用Pivotal Cloudfoundry集成你的应用程序。CloudFoundry是VMware推出的开源PaaS云平台。
            项目地址为: http://cloud.spring.io/spring-cloud-cloudfoundry。
        ·Spring Cloud Cloud Foundry Service Broker。
            为建立管理云托管服务的服务代理提供了一个起点。
            项目地址为: http://cloud.spring.io/spring-cloud-cloudfoundry-service-brokerl。
        ·Spring Cloud Cluster。
            基于Zookeeper、Redis、Hazelcast、Consul实现的领导选举与平民状态模式的抽象和实现。
            项目地址为: http://projects.spring.iolspring-cloud。
        ·Spring Cloud Consul。
            基于Hashicorp Consul实现的服务发现和配置管理。
            项目地址为: http://cloud.spring.io/spring-cloud-consul。
        ·Spring Cloud Security。
            在Zuul代理中为OAuth2 REST客户端和认证头转发提供负载均衡。
            项目地址为: http://cloud.spring.io/spring-cloud-security。
        ·Spring Cloud Sleuth。
            适用于Spring Cloud应用程序的分布式跟踪，与Zipkin、HTrace和基于日志（如ELK）的跟踪相兼容。可以用于日志的收集。
            项目地址为: http://cloud.spring.io/spring-cloud-sleuth。
        ·Spring Cloud Data Flow。
            一种针对现代运行时可组合的微服务应用程序的云本地编排服务。易于使用的DSL、拖放式GUI和RESTAPI一起简化基于微服务
            的数据管道的整体编排。
            项目地址为: http://cloud.spring.io/spring-cloud-dataflow。
        ·Spring Cloud Stream。
            一个轻量级的事件驱动的微服务框架来快速构建可以连接到外部系统的应用程序。使用Apache Kafka或RabbitMQ在Spring Boot
            应用程序之间发送和接收消息的简单声明模型。
            项目地址为: http://cloud.spring.iolspring-cloud-stream。
        ·Spring Cloud Stream App Starters。
            基于Spring Boot为外部系统提供Spring的集成。
            项目地址为:http://cloud.spring.io/spring-cloud-stream-app-starterso
        ·Spring Cloud Task。
            短生命周期的微服务——为Spring Boot应用简单声明添加功能和非功能特性。
            项目地址为: http://cloud.spring.io/spring-cloud-task。
        ·Spring Cloud Task App Starters。
            Spring Cloud Task App Starters是Spring Boot应用程序，可能是任何进程，包括Spring Batch作业，并可以在数据处理
            有限的时间终止。
            项目地址为: http://cloud.spring.io/spring-cloud-config。
        ·Spring Cloud Zookeeper。
            基于Apache Zookeeper的服务发现和配置管理的工具包，用于使用Zookeeper方式的服务注册和发现。
            项目地址为: http://cloud.spring.io/spring-cloud-task-app-starters。
        ·Spring Cloud for Amazon Web Services。
            与Amazon Web Services轻松集成。它提供了一种方便的方式来与AWS提供的服务进行交互，使用众所周知的 Spring 惯用语
            和API(如消息传递或缓存API)。开发人员可以围绕托管服务构建应用程序，而无须关心基础设施或维护工作。
            项目地址为: https://cloud.spring.io/spring-cloud-aws。
        ·Spring Cloud Connectors。
            便于PaaS应用在各种平台上连接到后端，如数据库和消息服务
            项目地址为: http://cloud.spring.io/spring-cloud-config。
        ·Spring Cloud Starters。
            基于Spring Boot的项目，用以简化Spring Cloud的依赖管理。该项目已经终止，并且在Angel.SR2后的版本和其他项目合并。
            项目地址为: https://cloud.spring.io/spring-cloud-connectors。
        ·Spring Cloud CLI。
            Spring Boot CLI插件用于在Groovy中快速创建Spring Cloud组件应用程序。
            项目地址为: https://lgithub.com/spring-cloud/spring-cloud-cli。
        ·Spring Cloud Contract。
            Spring Cloud Contract是一个总体项目，其中包含帮助用户成功实施消费者驱动契约(Consumer Driven Contracts)的解决方案。
            项目地址为:http://cloud.spring.io/spring-cloud-contract。

### 5-3-2、Spring Cloud 组件的版本
    Spring Cloud组件的详细版本对应关系
    | 组件                 | Camden.SR7    | Dalston.SR4   | Edgware.M1    | Finchley.M2 | Finchley.BUILD-SNAPSHOT |
    |spring-cloud-aws      | 1.1.4.RELEASE | 1.2.1.RELEASE | 1.2.1.RELEASE | 2.0.0.M1    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-bus      | 1.2.2.RELEASE | 1.3.1.RELEASE | 1.3.1.RELEASE | 2.0.0.M1    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-cli      | 1.2.4.RELEASE | 1.3.4.RELEASE | 1.4.0.M1      | 2.0.0.M1    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-commons  | 1.1.9.RELEASE | 1.2.4.RELEASE | 1.3.0.M1      | 2.0.0.M2    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-contract | 1.0.5.RELEASE | 1.1.4.RELEASE | 1.2.0.M1      | 2.0.0.M2    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-config   | 1.2.3.RELEASE | 1.3.3.RELEASE | 1.4.0.M1      | 2.0.0.M2    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-netflix  | 1.2.7.RELEASE | 1.3.5.RELEASE | 1.4.0.M1      | 2.0.0.M2    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-security | 1.1.4.RELEASE | 1.2.1.RELEASE | 1.2.1.RELEASE | 2.0.0.M1    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-cloudfoundry | 1.0.1.RELEASE | 1.1.0.RELEASE | 1.1.0.RELEASE | 2.0.0.M1    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-consul   | 1.1.4.RELEASE | 1.2.1.RELEASE | 1.2.1.RELEASE | 2.0.0.M1    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-sleuth   | 1.1.3.RELEASE | 1.2.5.RELEASE | 1.3.0.M1      | 2.0.0.M2    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-stream   | Brooklyn.SR3  | Chelsea.SR2   | Ditmars.M2    | Elmhurst.M1 | Elmhurst.BUILD-SNAPSHOT |
    |spring-cloud-zookeeper| 1.0.4.RELEASE | 1.1.2.RELEASE | 1.2.0.M1      | 2.0.0.M1    | 2.0.0.BUILD-SNAPSHOT               |
    |spring-boot           | 1.4.5.RELEASE | 1.5.4.RELEASE | 1.5.6.RELEASE | 2.0.0.M3    | 2.0.0.M3                |
    |spring-cloud-task     | 1.0.3.RELEASE | 1.1.2.RELEASE | 1.2.0.RELEASE | 2.0.0.M1    | 2.0.0.RELEASE           |
    |spring-cloud-vault    | --            | 1.0.2.RELEASE | 1.0.0.M1      | 2.0.0.M2    | 2.0.0.BUILD-SNAPSHOT    |
    |spring-cloud-gateway  | --            | --            | 1.0.0.M1      | 2.0.0.M2    | 2.0.0.BUILD-SNAPSHOT    |  




