# SpringBoot与SpringCloud版本对应关系
## 版本获取时间2020年12月
    {
        "git": {
            "branch": "72c34dc483d5e405f1fbc603084ffbd0a8c7aaf9",
            "commit": {
                "id": "72c34dc",
                "time": "2020-12-17 06:44:06"
            }
        },
        "build": {
            "version": "0.0.1-SNAPSHOT",
            "artifact": "start-site",
            "versions": {
                "spring-boot": "2.4.1",
                "initializr": "0.10.0-SNAPSHOT"
            },
            "name": "start.spring.io website",
            "time": "2020-12-17 06:51:05",
            "group": "io.spring.start"
        },
        "bom-ranges": {
            "azure": {
                "2.0.10": "Spring Boot >=2.0.0.RELEASE and <2.1.0.RELEASE",
                "2.1.10": "Spring Boot >=2.1.0.RELEASE and <2.2.0.M1",
                "2.2.4": "Spring Boot >=2.2.0.M1 and <2.3.0.M1",
                "2.3.5": "Spring Boot >=2.3.0.M1"
            },
            "codecentric-spring-boot-admin": {
                "2.0.6": "Spring Boot >=2.0.0.M1 and <2.1.0.M1",
                "2.1.6": "Spring Boot >=2.1.0.M1 and <2.2.0.M1",
                "2.2.4": "Spring Boot >=2.2.0.M1 and <2.3.0.M1",
                "2.3.1": "Spring Boot >=2.3.0.M1 and <2.5.0-M1"
            },
            "solace-spring-boot": {
                "1.0.0": "Spring Boot >=2.2.0.RELEASE and <2.3.0.M1",
                "1.1.0": "Spring Boot >=2.3.0.M1 and <2.5.0-M1"
            },
            "solace-spring-cloud": {
                "1.0.0": "Spring Boot >=2.2.0.RELEASE and <2.3.0.M1",
                "1.1.1": "Spring Boot >=2.3.0.M1 and <2.5.0-M1"
            },
            "spring-cloud": {
                "Finchley.M2": "Spring Boot >=2.0.0.M3 and <2.0.0.M5",
                "Finchley.M3": "Spring Boot >=2.0.0.M5 and <=2.0.0.M5",
                "Finchley.M4": "Spring Boot >=2.0.0.M6 and <=2.0.0.M6",
                "Finchley.M5": "Spring Boot >=2.0.0.M7 and <=2.0.0.M7",
                "Finchley.M6": "Spring Boot >=2.0.0.RC1 and <=2.0.0.RC1",
                "Finchley.M7": "Spring Boot >=2.0.0.RC2 and <=2.0.0.RC2",
                "Finchley.M9": "Spring Boot >=2.0.0.RELEASE and <=2.0.0.RELEASE",
                "Finchley.RC1": "Spring Boot >=2.0.1.RELEASE and <2.0.2.RELEASE",
                "Finchley.RC2": "Spring Boot >=2.0.2.RELEASE and <2.0.3.RELEASE",
                "Finchley.SR4": "Spring Boot >=2.0.3.RELEASE and <2.0.999.BUILD-SNAPSHOT",
                "Finchley.BUILD-SNAPSHOT": "Spring Boot >=2.0.999.BUILD-SNAPSHOT and <2.1.0.M3",
                "Greenwich.M1": "Spring Boot >=2.1.0.M3 and <2.1.0.RELEASE",
                "Greenwich.SR6": "Spring Boot >=2.1.0.RELEASE and <2.1.999.BUILD-SNAPSHOT",
                "Greenwich.BUILD-SNAPSHOT": "Spring Boot >=2.1.999.BUILD-SNAPSHOT and <2.2.0.M4",
                "Hoxton.SR9": "Spring Boot >=2.2.0.M4 and <2.3.8.BUILD-SNAPSHOT",
                "Hoxton.BUILD-SNAPSHOT": "Spring Boot >=2.3.8.BUILD-SNAPSHOT and <2.4.0.M1",
                "2020.0.0-M3": "Spring Boot >=2.4.0.M1 and <=2.4.0.M1",
                "2020.0.0-M4": "Spring Boot >=2.4.0.M2 and <=2.4.0-M3",
                "2020.0.0-RC1": "Spring Boot >=2.4.0.M4 and <2.4.2-SNAPSHOT",
                "2020.0.0-SNAPSHOT": "Spring Boot >=2.4.2-SNAPSHOT"
            },
            "spring-cloud-alibaba": {
                "2.2.1.RELEASE": "Spring Boot >=2.2.0.RELEASE and <2.3.0.M1"
            },
            "spring-cloud-gcp": {
                "2.0.0-RC2": "Spring Boot >=2.4.0-M1 and <2.5.0-M1"
            },
            "spring-cloud-services": {
                "2.0.3.RELEASE": "Spring Boot >=2.0.0.RELEASE and <2.1.0.RELEASE",
                "2.1.8.RELEASE": "Spring Boot >=2.1.0.RELEASE and <2.2.0.RELEASE",
                "2.2.6.RELEASE": "Spring Boot >=2.2.0.RELEASE and <2.3.0.RELEASE",
                "2.3.0.RELEASE": "Spring Boot >=2.3.0.RELEASE and <2.4.0-M1"
            },
            "spring-geode": {
                "1.2.12.RELEASE": "Spring Boot >=2.2.0.M5 and <2.3.0.M1",
                "1.3.7.RELEASE": "Spring Boot >=2.3.0.M1 and <2.4.0-M1",
                "1.4.0": "Spring Boot >=2.4.0-M1"
            },
            "spring-statemachine": {
                "2.0.0.M4": "Spring Boot >=2.0.0.RC1 and <=2.0.0.RC1",
                "2.0.0.M5": "Spring Boot >=2.0.0.RC2 and <=2.0.0.RC2",
                "2.0.1.RELEASE": "Spring Boot >=2.0.0.RELEASE"
            },
            "vaadin": {
                "10.0.17": "Spring Boot >=2.0.0.M1 and <2.1.0.M1",
                "14.4.4": "Spring Boot >=2.1.0.M1 and <2.5.0-M1"
            },
            "wavefront": {
                "2.0.2": "Spring Boot >=2.1.0.RELEASE and <2.4.0-M1",
                "2.1.0-RC1": "Spring Boot >=2.4.0-M1 and <2.4.2-SNAPSHOT",
                "2.1.0-SNAPSHOT": "Spring Boot >=2.4.2-SNAPSHOT"
            }
        },
        "dependency-ranges": {
            "okta": {
                "1.2.1": "Spring Boot >=2.1.2.RELEASE and <2.2.0.M1",
                "1.4.0": "Spring Boot >=2.2.0.M1 and <2.4.0-M1",
                "1.5.1": "Spring Boot >=2.4.0-M1 and <2.5.0-M1"
            },
            "mybatis": {
                "2.0.1": "Spring Boot >=2.0.0.RELEASE and <2.1.0.RELEASE",
                "2.1.4": "Spring Boot >=2.1.0.RELEASE and <2.5.0-M1"
            },
            "camel": {
                "2.22.4": "Spring Boot >=2.0.0.M1 and <2.1.0.M1",
                "2.25.2": "Spring Boot >=2.1.0.M1 and <2.2.0.M1",
                "3.3.0": "Spring Boot >=2.2.0.M1 and <2.3.0.M1",
                "3.5.0": "Spring Boot >=2.3.0.M1 and <2.4.0-M1",
                "3.7.0": "Spring Boot >=2.4.0.M1 and <2.5.0-M1"
            },
            "open-service-broker": {
                "2.1.3.RELEASE": "Spring Boot >=2.0.0.RELEASE and <2.1.0.M1",
                "3.0.4.RELEASE": "Spring Boot >=2.1.0.M1 and <2.2.0.M1",
                "3.1.1.RELEASE": "Spring Boot >=2.2.0.M1 and <2.4.0-M1"
            }
        }
    }