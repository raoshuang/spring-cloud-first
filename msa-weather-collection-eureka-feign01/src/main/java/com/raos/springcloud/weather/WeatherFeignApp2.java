package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/12 22:18
 * 基于Gradle构建的天气服务--基础服务eureka--feign客户端工程入口
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class WeatherFeignApp2 {

	public static void main(String[] args) {
		SpringApplication.run(WeatherFeignApp2.class, args);
	}

}
