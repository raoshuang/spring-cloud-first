package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/6 20:52
 * 基于Gradle构建的天气服务工程入口
 */
@SpringBootApplication
public class WeatherApp3 {

	public static void main(String[] args) {
		SpringApplication.run(WeatherApp3.class, args);
	}

}
