package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.Weather;
import com.raos.springcloud.weather.vo.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/9 21:07
 * 天气预报服务实现.
 */
@Service
public class WeatherReportServiceImpl implements WeatherReportService {

	@Autowired
	private WeatherDataService weatherDataServiceImpl;
	
	@Override
	public Weather getDataByCityId(String cityId) {
		WeatherResponse result = weatherDataServiceImpl.getDataByCityId(cityId);
		return result.getData();
	}

}
