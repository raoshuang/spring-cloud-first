package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.WeatherResponse;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/8 21:12
 * 天气数据服务接口
 */
public interface WeatherDataService {

    /**
     * 根据城市ID查询天气数据
     * @param cityId 城市ID
     * @return 天气数据
     */
    WeatherResponse getDataByCityId(String cityId);

    /**
     * 根据城市名称查询天气数据
     * @param cityName 城市名称
     * @return 天气数据
     */
    WeatherResponse getDataByCityName(String cityName);

    /**
     * 根据城市ID同步天气数据
     * @param cityId 城市ID
     */
    void syncDataByCityId(String cityId);
}
