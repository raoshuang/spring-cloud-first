package com.raos.springcloud.weather.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/8 23:12
 * 城市类
 */
@XmlRootElement(name = "d")
@XmlAccessorType(XmlAccessType.FIELD)
public class City {

	@XmlAttribute(name = "d1")
	private String cityId;

	@XmlAttribute(name = "d2")
	private String cityName;

	@XmlAttribute(name = "d3")
	private String cityCode;

	@XmlAttribute(name = "d4")
	private String province;

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "City{" +
				"cityId='" + cityId + '\'' +
				", cityName='" + cityName + '\'' +
				", cityCode='" + cityCode + '\'' +
				", province='" + province + '\'' +
				'}';
	}
}
