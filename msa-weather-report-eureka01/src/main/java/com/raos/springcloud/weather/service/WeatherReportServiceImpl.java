package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.Forecast;
import com.raos.springcloud.weather.vo.Weather;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/10 22:07
 * 天气预报服务实现.
 */
@Service
public class WeatherReportServiceImpl implements WeatherReportService {
	
	@Override
	public Weather getDataByCityId(String cityId) {
		// TODO 改为由天气数据API微服务来提供数据
		Weather data = new Weather();
		data.setAqi("81");
		data.setCity("深圳");
		data.setGanmao("各项气象条件适宜，无明显降温过程，发生感冒机率较低。");
		data.setWendu("22");

		List<Forecast> forecastList = new ArrayList<>();
		Forecast forecast =new Forecast();
		forecast.setDate("29日星期天");
		forecast.setType("多云");
		forecast.setFengxiang("无持续风向1");
		forecast.setHigh("高温1 27℃");
		forecast.setLow("低温1 20℃");
		forecastList.add(forecast);

		forecast =new Forecast();
		forecast.setDate("29日星期天");
		forecast.setType("多云");
		forecast.setFengxiang("无持续风向2");
		forecast.setHigh("高温2 27℃");
		forecast.setLow("低温2 20℃");
		forecastList.add(forecast);

		forecast =new Forecast();
		forecast.setDate("30日星期一");
		forecast.setType("多云");
		forecast.setFengxiang("无持续风向3");
		forecast.setHigh("高温3 27℃");
		forecast.setLow("低温3 20℃");
		forecastList.add(forecast);

		forecast =new Forecast();
		forecast.setDate("31日星期二");
		forecast.setType("多云");
		forecast.setFengxiang("无持续风向4");
		forecast.setHigh("高温4 27℃");
		forecast.setLow("低温4 20℃");
		forecastList.add(forecast);

		forecast =new Forecast();
		forecast.setDate("1日星期三");
		forecast.setType("多云");
		forecast.setFengxiang("无持续风向5");
		forecast.setHigh("高温5 27℃");
		forecast.setLow("低温5 20℃");
		forecastList.add(forecast);

		forecast =new Forecast();
		forecast.setDate("2日星期四");
		forecast.setType("多云");
		forecast.setFengxiang("无持续风向6");
		forecast.setHigh("高温6 27℃");
		forecast.setLow("低温6 20℃");
		forecastList.add(forecast);

		data.setForecast(forecastList);
		return data;
	}

}
