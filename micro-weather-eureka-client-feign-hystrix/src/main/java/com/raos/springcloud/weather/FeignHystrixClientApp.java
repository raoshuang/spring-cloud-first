package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/17 22:34
 * 基于Gradle构建的Spring Boot Eureka Feign Hystrix客户端入口
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableCircuitBreaker
public class FeignHystrixClientApp {

	public static void main(String[] args) {
		SpringApplication.run(FeignHystrixClientApp.class, args);
	}

}
