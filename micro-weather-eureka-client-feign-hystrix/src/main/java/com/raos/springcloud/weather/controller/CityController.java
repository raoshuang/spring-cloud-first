package com.raos.springcloud.weather.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.raos.springcloud.weather.service.CityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/17 21:40
 * City Controller.
 */
@RestController
public class CityController {

    @Autowired
    private CityClient cityClient;

    @GetMapping("/cities")
    @HystrixCommand(fallbackMethod = "defaultCities")
    public String listCity() {
        // 通过Feign客户端来查找
        String body = cityClient.listCity();
        return body;
    }

    /** 自定义断路器默认返回的内容*/
    public String defaultCities() {
        return "城市数据API服务暂时不可用！";
    }
}
