package com.raos.springcloud.weather.controller;

import com.raos.springcloud.weather.service.WeatherDataService;
import com.raos.springcloud.weather.vo.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/8 21:25
 * 天气API
 */
@RestController
@RequestMapping("/weather")
public class WeatherController {

    @Autowired
    private WeatherDataService weatherDataService;

    /**
     * 根据城市ID查询天气数据
     * @param cityId 城市id
     */
    @GetMapping("/cityId/{cityId}")
    public WeatherResponse getReportByCityId(@PathVariable("cityId") String cityId) {
        return weatherDataService.getDataByCityId(cityId);
    }

    /**
     * 根据城市名称查询天气数据
     * @param cityName 城市名称
     */
    @GetMapping("/cityName/{cityName}")
    public WeatherResponse getReportByCityName(@PathVariable("cityName") String cityName) {
        return weatherDataService.getDataByCityName(cityName);
    }
}
