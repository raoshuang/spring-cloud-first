package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.Weather;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/10 22:15
 * 天气预报服务接口测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherReportServiceTest {
	
	@Autowired
	private WeatherReportService weatherReportService;
	
	@Test
	public void testGetDataByCityId() {
		Weather weather = weatherReportService.getDataByCityId("101280601");
		assertEquals("Get Data By City Id", "深圳",  weather.getCity());
	}

}
