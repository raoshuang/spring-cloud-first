package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.WeatherResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/12 22:39
 * 访问天气数据的客户端.
 */
@FeignClient("msa-weather-data-eureka01")
public interface WeatherDataClient {

	/**
	 * 根据城市ID查询天气数据
	 */	
	@GetMapping("/weather/cityId/{cityId}")
	WeatherResponse getDataByCityId(@PathVariable("cityId") String cityId);
}
