package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.Weather;
import com.raos.springcloud.weather.vo.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/12 22:41
 * 天气预报服务实现.
 */
@Service
public class WeatherReportServiceImpl implements WeatherReportService {

	@Autowired
	private WeatherDataClient weatherDataClient;

	@Override
	public Weather getDataByCityId(String cityId) {
		WeatherResponse weatherResponse = weatherDataClient.getDataByCityId(cityId);
		return weatherResponse.getData();
	}

}
