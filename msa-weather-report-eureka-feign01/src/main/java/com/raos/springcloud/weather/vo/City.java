package com.raos.springcloud.weather.vo;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/8 23:12
 * 城市类
 */
public class City {

	private String cityId;
	private String cityName;
	private String cityCode;
	private String province;

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "City{" +
				"cityId='" + cityId + '\'' +
				", cityName='" + cityName + '\'' +
				", cityCode='" + cityCode + '\'' +
				", province='" + province + '\'' +
				'}';
	}
}
