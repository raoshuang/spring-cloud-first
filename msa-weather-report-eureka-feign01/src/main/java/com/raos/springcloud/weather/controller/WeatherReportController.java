package com.raos.springcloud.weather.controller;

import com.raos.springcloud.weather.service.CityClient;
import com.raos.springcloud.weather.service.WeatherReportService;
import com.raos.springcloud.weather.vo.City;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/12 22:54
 * 天气预报API.
 */
@RestController
@RequestMapping("/report")
public class WeatherReportController {
	private static final Logger logger = LoggerFactory.getLogger(WeatherReportController.class);

	@Autowired
	private CityClient cityClient;
	@Autowired
	private WeatherReportService weatherReportService;

	@GetMapping("/cityId/{cityId}")
	public ModelAndView getReportByCityId(@PathVariable("cityId") String cityId, Model model) throws Exception {
		// 由城市数据API微服务来提供数据
		List<City> cityList = null;
		try {
			// 调用城市数据API
			cityList = cityClient.listCity();
		} catch (Exception e) {
			logger.error("获取城市信息异常！", e);
			throw new RuntimeException("获取城市信息异常！", e);
		}

		model.addAttribute("title", "宇峰的天气预报");
		model.addAttribute("cityId", cityId);
		model.addAttribute("cityList", cityList);
		model.addAttribute("report", weatherReportService.getDataByCityId(cityId));
		return new ModelAndView("weather/report", "reportModel", model);
	}

}
