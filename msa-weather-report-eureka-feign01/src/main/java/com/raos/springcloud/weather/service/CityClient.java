package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.City;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/12 22:29
 *  访问城市信息的客户端.
 */
@FeignClient("msa-weather-city-eureka01")
public interface CityClient {
    /**
     * 获取城市信息列表
     */
    @GetMapping("/cities")
    List<City> listCity() throws Exception;
}
