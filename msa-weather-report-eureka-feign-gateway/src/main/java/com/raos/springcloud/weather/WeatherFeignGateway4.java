package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/13 22:08
 * 基于Gradle构建的天气服务-天气预报服务eureka--feign--zuul网关工程入口
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class WeatherFeignGateway4 {

	public static void main(String[] args) {
		SpringApplication.run(WeatherFeignGateway4.class, args);
	}

}
