package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.City;
import com.raos.springcloud.weather.vo.WeatherResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/13 22:15
 * 访问数据的客户端.
 */
@FeignClient("msa-weather-eureka-client-zuul")
public interface DataClient {

	/**
	 * 获取城市列表
	 */
	@GetMapping("/city/cities")
	List<City> listCity() throws Exception;
	
	/**
	 * 根据城市ID查询天气数据
	 */	
	@GetMapping("/data/weather/cityId/{cityId}")
	WeatherResponse getDataByCityId(@PathVariable("cityId") String cityId);
}
