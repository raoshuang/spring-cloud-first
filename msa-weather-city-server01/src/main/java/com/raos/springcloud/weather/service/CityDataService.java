package com.raos.springcloud.weather.service;

import com.raos.springcloud.weather.vo.City;
import java.util.List;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/8 23:22
 * 城市数据服务接口.
 */
public interface CityDataService {

	/**
	 * 获取城市列表.
	 */
	List<City> listCity() throws Exception;
}
