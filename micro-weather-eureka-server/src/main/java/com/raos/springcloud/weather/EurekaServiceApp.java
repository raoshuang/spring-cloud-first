package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/11 21:04
 * 基于Gradle构建的Spring Boot Eureka服务端入口
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServiceApp {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServiceApp.class, args);
	}

}
