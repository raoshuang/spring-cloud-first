package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/12 21:04
 * 基于Gradle构建的Spring Boot Eureka Ribbon客户端入口
 */
@SpringBootApplication
@EnableDiscoveryClient
public class RibbonClientApp {

	public static void main(String[] args) {
		SpringApplication.run(RibbonClientApp.class, args);
	}

}
