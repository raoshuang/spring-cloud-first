package com.raos.springcloud.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/12 21:20
 * City Controller.
 */
@RestController
public class CityController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/cities")
    public String listCity() {
        // 通过应用名词来查找
        String body = restTemplate.getForEntity("http://msa-weather-city-eureka01/cities", String.class).getBody();
        return body;
    }
}
