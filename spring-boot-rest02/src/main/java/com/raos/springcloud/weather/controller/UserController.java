package com.raos.springcloud.weather.controller;

import com.raos.springcloud.weather.domain.User;
import com.raos.springcloud.weather.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/6 22:46
 * 用户Controller
 */
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    /**
     * 获取用户列表
     */
    @GetMapping
    public List<User> getUsers() {
        return userRepository.listUser();
    }

    /**
     * 获取用户信息
     * @param id 用户id
     */
    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") Long id) {
        return userRepository.getUserById(id);
    }

    /**
     * 保存用户
     * @param user 用户实体信息
     */
    @PostMapping
    public User createUser(@RequestBody User user) {
        return userRepository.saveOrUpateUser(user);
    }

    /**
     * 修改用户
     * @param id 用户
     * @param user 用户信息
     */
    @PutMapping("/{id}")
    public void updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        User oldUser = this.getUser(id);
        if (oldUser != null) {
            user.setId(id);
            userRepository.saveOrUpateUser(user);
        }
    }

    /**
     * 删除用户
     * @param id 用户id
     */
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userRepository.deleteUser(id);
    }
}
