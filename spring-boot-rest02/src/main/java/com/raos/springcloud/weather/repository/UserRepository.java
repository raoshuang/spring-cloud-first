package com.raos.springcloud.weather.repository;

import com.raos.springcloud.weather.domain.User;

import java.util.List;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/6 22:39
 * 用户Repository接口
 */
public interface UserRepository {
    /**
     * 新增或者修改用户
     */
    User saveOrUpateUser(User user);
    /**
     * 删除用户
     */
    void deleteUser(Long id);
    /**
     * 根据用户id获取用户
     */
    User getUserById(Long id);
    /**
     * 获取所有用户的列表
     */
    List<User> listUser();
}
