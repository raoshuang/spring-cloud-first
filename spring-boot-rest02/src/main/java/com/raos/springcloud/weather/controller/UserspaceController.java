package com.raos.springcloud.weather.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/6 23:16
 * 配置文件自定义属性注入
 */
@RestController
@RequestMapping("/u")
public class UserspaceController {
    @Value("${file.server.url}")
    private String fileServerUrl;

    @GetMapping( "/{username}/edit")
    public String createBlog (@PathVariable("username")String username) {
        Map<String, Object> data = new HashMap<>(8);
        data.put("fileServerUrl", fileServerUrl);
        data.put("username", username);
        return data.toString();
    }
}
