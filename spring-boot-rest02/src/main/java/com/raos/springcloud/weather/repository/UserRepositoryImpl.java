package com.raos.springcloud.weather.repository;

import com.raos.springcloud.weather.domain.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/6 22:39
 * 用户Repository接口实现类
 */
@Repository
public class UserRepositoryImpl implements UserRepository{
    private static AtomicLong counter = new AtomicLong();
    private final ConcurrentMap<Long, User> userMap = new ConcurrentHashMap<>();

    @Override
    public User saveOrUpateUser(User user) {
        Long id = user.getId();
        if (id == null || id <= 0) {
            id = counter.incrementAndGet();
            user.setId(id);
        }
        this.userMap.put(id, user);
        return this.getUserById(id);
    }

    @Override
    public void deleteUser(Long id) {
        this.userMap.remove(id);
    }

    @Override
    public User getUserById(Long id) {
        return this.userMap.get(id);
    }

    @Override
    public List<User> listUser() {
        return new ArrayList<>(this.userMap.values());
    }
}
