package com.raos.springcloud;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/6 20:52
 * 主应用测试入口
 */
@SpringBootTest
class RestApplicationTests {

	@Test
	void contextLoads() {
	}

}
