package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/13 21:04
 * 基于Gradle构建的Spring Boot Eureka--Zuul网关客户端入口
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class EurekaZuulClientApp {

	public static void main(String[] args) {
		SpringApplication.run(EurekaZuulClientApp.class, args);
	}

}
