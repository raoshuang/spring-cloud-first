package com.raos.springcloud.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author RaoShuang
 * @emil 991207823@qq.com
 * @date 2020/11/11 22:18
 * 基于Gradle构建的天气服务--基础服务eureka客户端工程入口
 */
@SpringBootApplication
@EnableDiscoveryClient
public class WeatherServiceApp1 {

	public static void main(String[] args) {
		SpringApplication.run(WeatherServiceApp1.class, args);
	}

}
